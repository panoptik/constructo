<?php

class m140324_090511_initial extends CDbMigration
{
	public function up()
	{
        // user
        $this->createTable('user', array(
            'id' => 'pk',
            'email' => 'varchar(255) NOT NULL',
            'password' => 'varchar(255) NOT NULL',
            'role' => 'varchar(50) NOT NULL', // role may be (admin, customer(user), partner)
            'status' => 'tinyint(4) NOT NULL', // active / banned / pending
            'firstName' => 'varchar(255) NOT NULL',
            'lastName' => 'varchar(255) NOT NULL',
            'facebookId' => 'varchar(100) NOT NULL', // facebook id if user log in or register from fb
            'image' => 'varchar(255) NOT NULL',
            'birthDate' => 'date NOT NULL',
            'createdAt' => 'datetime NOT NULL',
            'updatedAt' => 'datetime NOT NULL',
        ), 'ENGINE=InnoDb');
        $this->createIndex('u_user_email', 'user', 'email', true);

        $this->insert('user', array(
            'id'=>1,
            'email' => 'admin@panoptik.net',
            'password' => CPasswordHelper::hashPassword('admin37'),
            'role' => 'admin',
            'status' => '1',
            'firstName' => 'John',
            'lastName' => 'Dou',
            'facebookId' => '',
            'image' => '',
            'birthDate' => '1970-01-01',
            'createdAt' => new CDbExpression('NOW()'),
            'updatedAt' => new CDbExpression('NOW()'),
        ));

        // related images
        $this->createTable('image', array(
            'id' => 'pk',
            'model' => 'varchar(255) NOT NULL',
            'relationId' => 'int(11) NOT NULL',
            'image' => 'varchar(255) NOT NULL',
            'createdAt' => 'datetime NOT NULL',
        ), 'ENGINE=InnoDb');

        // keep all translated model properties (properties not exists in such models)
        $this->createTable('translate', array(
            'model' => 'varchar(255) NOT NULL',
            'relationId' => 'int(11) NOT NULL',
            'language' => 'varchar(20) NOT NULL',
            'key' => 'varchar(255) NOT NULL',
            'value' => 'text NOT NULL',
        ), 'ENGINE=InnoDb');
        $this->addPrimaryKey('pk_translate', 'translate', 'model, relationId, language, key');

        // custom post pages (articles, news e.g.)
        // it would be great to merge this tables
        // but maybe in the future it will have new specific attributes (tags, categories e.g)
        // title, content, seo fields keep in translation table
        $this->createTable('post', array(
            'id' => 'pk',
            'name' => 'varchar(255) NOT NULL',
            'status' => 'tinyint(4) NOT NULL', // active / inactive
            'alias' => 'varchar(255) NOT NULL', // unique url alias
            'createdAt' => 'datetime NOT NULL',
            'updatedAt' => 'datetime NOT NULL',
            'type' => 'tinyint(4) NOT NULL', // page type (post, static page, news, article etc)
        ), 'ENGINE=InnoDb');
        $this->createIndex('u_post_alias', 'post', 'alias', true);
	}

	public function down()
	{
        $this->dropIndex('u_post_alias', 'post');
        $this->dropTable('post');

        $this->dropTable('translate');

        $this->dropTable('image');

        $this->dropIndex('u_user_email', 'user');
        $this->dropTable('user');
	}
}