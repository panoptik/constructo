<?php

class m140327_130632_create_settings_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('settings', array(
            'id' => 'pk',
            'key' => 'varchar(255) NOT NULL',
            'value' => 'text NOT NULL',
            'description' => 'varchar(255) NOT NULL',
            'group' => 'varchar(255) NOT NULL',
            'type' => 'varchar(255) NOT NULL',
            'sort' => 'int(8) NOT NULL',
        ), 'ENGINE=InnoDb');
	}

	public function down()
	{
		$this->dropTable('settings');
	}
}