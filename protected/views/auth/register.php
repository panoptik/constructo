<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 13:14
 */
?>

<?=$this->renderPartial('socials');?>

<div class="form">
    <form id="registration-form" method="POST" action="/auth/register" class="" autocomplete="off" name="registerForm">

        <div class="row">
            <?= CHtml::button('Register with Facebook',array('class'=>'btn fb')); ?>
        </div>

        <div class="row">
            <p>or fill the form below</p>
        </div>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <div class="row">
            <?= CHtml::activeLabel($model, 'firstName');?>
            <?= CHtml::activeTextField($model, 'firstName'); ?>
            <?= CHtml::error($model, 'firstName'); ?>
        </div>

        <div class="row">
            <?= CHtml::activeLabel($model, 'lastName');?>
            <?= CHtml::activeTextField($model, 'lastName'); ?>
            <?= CHtml::error($model, 'lastName'); ?>
        </div>

        <div class="row">
            <?= CHtml::activeLabel($model, 'email');?>
            <?= CHtml::activeTextField($model, 'email'); ?>
            <?= CHtml::error($model, 'email'); ?>
        </div>

        <div class="row">
            <?= CHtml::activeLabel($model, 'password');?>
            <?= CHtml::activePasswordField($model, 'password'); ?>
            <?= CHtml::error($model, 'password'); ?>
        </div>

        <div class="row">
            <?= CHtml::activeLabel($model, 'passwordConfirm');?>
            <?= CHtml::activePasswordField($model, 'passwordConfirm'); ?>
            <?= CHtml::error($model, 'passwordConfirm'); ?>
        </div>
        <?= CHtml::hiddenField('facebookToken', $facebookToken); ?>

        <div class="row buttons">
            <?= CHtml::submitButton('Register'); ?>
        </div>
    </form>
</div>