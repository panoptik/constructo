<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<?=$this->renderPartial('socials');?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="flash info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

    <div class="row buttons">
        <?php echo CHtml::button('Login with Facebook', array('class'=>'fb btn')); ?>
    </div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?=$form->labelEx($model,'email'); ?>
		<?=$form->textField($model,'email'); ?>
		<?=$form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?=$form->labelEx($model,'password'); ?>
		<?=$form->passwordField($model,'password'); ?>
		<?=$form->error($model,'password'); ?>
	</div>

	<div class="row rememberMe">
		<?=$form->checkBox($model,'rememberMe'); ?>
		<?=$form->label($model,'rememberMe'); ?>
		<?=$form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?=CHtml::submitButton('Login'); ?>
	</div>

    <div class="row">
        <?=CHtml::link('Registration',Yii::app()->createUrl('auth/register'), array('id'=>'registerUrl'));?>
    </div>

    <div class="row">
        <?=CHtml::link('Forgot password?',Yii::app()->createUrl('auth/passwordRecovery'));?>
    </div>
<?php $this->endWidget(); ?>

    <form method="post" action="<?=Yii::app()->createUrl('auth/register')?>" id="FB_register">
        <?=CHtml::hiddenField('FB[firstName]','');?>
        <?=CHtml::hiddenField('FB[lastName]','');?>
        <?=CHtml::hiddenField('FB[email]','');?>
        <?=CHtml::hiddenField('facebookToken','');?>
    </form>

</div><!-- form -->
