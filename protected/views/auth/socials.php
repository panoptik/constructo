<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 16:31
 */
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($this->assetUrl . '/js/auth.js', CClientScript::POS_HEAD);
?>

<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
    FB.init({
            appId: '<?=Yii::app()->params['facebook']['appId']?>',
            cookie: true,
            xfbml: true,
            oauth: true
        });
    };
(function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
}());
</script>