<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Password Recovery';
$this->breadcrumbs=array(
	'Password Recovery',
);
?>

<h1>Password Recovery</h1>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'restorePassword-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    <? foreach ($formRows as $row):?>
        <div class="row">
            <?= $form->labelEx($model, $row); ?>
            <?= $form->textField($model, $row); ?>
            <?= $form->error($model, $row); ?>
        </div>
    <? endforeach; ?>

    <? foreach ($formPassRows as $row):?>
        <div class="row">
            <?= $form->labelEx($model, $row); ?>
            <?= $form->passwordField($model, $row); ?>
            <?= $form->error($model, $row); ?>
        </div>
    <? endforeach; ?>


    <? foreach ($hiddenVals as $name=>$value):?>
        <?= $form->hiddenField($model, $name, array('value'=>$value)); ?>
    <? endforeach; ?>

    <? if ($userNotice):?>
        <p> <?= $userNotice ?> </p>
    <? endif; ?>

    <? if ($wrongHash): ?>
        <p> Wrong or expired recovery URL </p>
    <? endif; ?>

    <? if (!empty($formRows) || !empty($formPassRows)): ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Пыщ'); ?>
	</div>
    <? endif; ?>

<?php $this->endWidget(); ?>

</div><!-- form -->
