<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 28.03.14
 * Time: 12:09
 */
?>
Dear <?= $userInfo->firstName ?>,<br/>
If you haven't asked for password recovery - just ignore this email. <br/><br/>

<?=CHtml::link('Click Here to Restore Your Password', $this->createAbsoluteUrl('auth/passwordRecovery',array('hash'=>$hash)));