<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 10:44
 */

class FrontController extends Controller {

    public $assetUrl;

    public function init() {
        parent::init();
        $am = Yii::app()->assetManager;
        $assetDir = Yii::getPathOfAlias('application.components.assets');
        $this->assetUrl = $am->publish( $assetDir, false, -1, YII_DEBUG );
    }
} 