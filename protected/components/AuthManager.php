<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 13:08
 */

class AuthManager extends CPhpAuthManager{
    public function init(){

        if($this->authFile===null){
            $this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';
        }

        parent::init();

        if(!Yii::app()->user->isGuest){
            $this->assign(Yii::app()->user->role, Yii::app()->user->id);
        }
    }
}