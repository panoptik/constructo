<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

    protected $_id;
    protected $_role;

    public function authenticate($checkPass = true){
        $user = User::model()->findByAttributes(array(),'email=:email', array(':email'=>strtolower($this->username)));
        if($user===null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (!CPasswordHelper::verifyPassword($this->password, $user->password) && $checkPass) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->_id = $user->id;
            $this->username = $user->firstName . ' ' . $user->lastName;
            $this->role = $user->role;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId(){
        return $this->_id;
    }

    public function getRole(){
        return $this->_role;
    }

    public function setRole($value){
        $this->_role = $value;
    }
}