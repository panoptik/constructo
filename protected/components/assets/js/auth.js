function alertMessage($text){
    alert($text);
}

function fb_login() {
    FB.login(function(response) {
        $.ajax({
            url: baseUrl + '/auth/facebook/',
            type: 'POST',
            data: {'authResponse' : response.authResponse },
            dataType: 'json',
            success: function(data){
                console.log(data);
                if (data.success){
                    if (data.redirect){
                        window.location = data.redirect;
                        return true;
                    }
                } else {
                    if (data.need_registration){
                        if ($('#registerUrl').length){
                            $('#FB_firstName').val(data.userData.first_name);
                            $('#FB_lastName').val(data.userData.last_name);
                            $('#FB_email').val(data.userData.email);
                            $('#facebookToken').val(response.authResponse.accessToken);
                            $('#FB_register').submit();
                        } else {
                            $('#User_firstName').val(data.userData.first_name);
                            $('#User_lastName').val(data.userData.last_name);
                            $('#User_email').val(data.userData.email);
                            $('#facebookToken').val(response.authResponse.accessToken);
                            alertMessage('Enter Password to Finish Registration');
                        }
                    }
                }

            },
            error: function(data){
                console.log(data);
            }
        });
    }, {
        scope:'basic_info, email, public_profile'
    });
}

$(document).ready(function(){
    $(".btn.fb").on("click", function(){
        fb_login();
        return false;
    });
});
