<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Bogdan
 * Date: 12.08.13
 * Time: 17:58
  */

class ViewRenderer extends CApplicationComponent implements IViewRenderer {

    public $fileExtension = '.php';

    public $filePermission = 0755;

    public $useRuntimePath=true;


    public function renderFile($context, $sourceFile, $data, $return) {
        if(!is_file($sourceFile) || ($file=realpath($sourceFile))===false)
            throw new CException(Yii::t('yii','View file "{file}" does not exist.',array('{file}'=>$sourceFile)));
        $viewFile=$sourceFile;
        $data['this'] = $context;
        $data['baseUrl'] = Yii::app()->baseUrl;
        $data['currentUser'] = Yii::app()->user;
        $data['isGuest'] = Yii::app()->user->isGuest;
        return $context->renderInternal($viewFile,$data,$return);
    }

}