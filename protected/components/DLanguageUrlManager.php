<?php
/**
 * Created by PhpStorm.
 * User: senseless-long-name
 * Date: 3/28/14
 * Time: 3:54 PM
 */

class DLanguageUrlManager extends CUrlManager
{
    public function createUrl($route, $params=array(), $ampersand='&')
    {
        $url = parent::createUrl($route, $params, $ampersand);
        return DMultilangHelper::addLangToUrl($url);
    }
}