<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 16.06.13 Time: 16:00
 */

class ActiveRecord extends CActiveRecord {

    /**
     * translated fields which are being converted into relation
     * key represents current language property value
     * value represents all property values for each language
     * @var array
     */
    protected $translatedFields = array();

    /**
     * field for translations
     * @var array
     */
    public $translations = array();

    /**
     * @var array
     * list of attributes which files will be contain in image folder
     */
    protected $imageAttributes = array('image');

    /**
     * @var array
     * list of attributes which files will be contain in other files folder
     * for example mp3, doc, zip files
     */
    protected $fileAttributes = array();

    /**
     * @param $attribute
     * @return string
     * @throws CException
     * get model file attribute path and create if not exists folder structure
     */
    public function getAttributeDirAlias($attribute) {
        $path = Yii::app()->params->uploadFolder;
        if(in_array($attribute, $this->imageAttributes)) {
            $path .= '.image';
        } elseif(in_array($attribute, $this->fileAttributes)) {
            $path .= '.files';
        } else {
            $path .= '.' . strtolower($attribute);
        }

        $realPath = Yii::getPathOfAlias($path);
        if(!file_exists($realPath)) {
            if(!mkdir($realPath)) {
                throw new CException('Cannot create folder ' . $realPath . ' check if parent directory exists and is writable');
            }
            chmod($realPath, 0777);
        }
        $path .= '.' . strtolower(get_class($this));
        $realPath = Yii::getPathOfAlias($path);
        if(!file_exists($realPath)) {
            if(!mkdir($realPath)) {
                throw new CException('Cannot create folder ' . $realPath . ' check if parent directory exists and is writable');
            }
            chmod($realPath, 0777);
        }
        return $path;
    }

    /**
     * get attribute file directory path
     * @param string $attribute
     * @return string
     */
    public function getAttributeDirPath($attribute) {
        return Yii::getPathOfAlias($this->getAttributeDirAlias($attribute));
    }

    /**
     * get full file path to specified model attribute
     * @param string $attribute
     * @return string
     */
    public function getAttributePath($attribute) {
        return $this->getAttributeDirPath($attribute) . DIRECTORY_SEPARATOR . $this->$attribute;
    }

    /**
     * @param $attribute
     * @return string
     * convert attribute path to url
     */
    public function getFileUrl($attribute) {
        $path = $this->getAttributeDirAlias($attribute);
        return Tools::pathToUrl($path, $this->$attribute);
    }

    /**
     * save relation models
     * each key represent attribute with data to save
     * value one of string or array
     * if string specified
     * relation model must have relationId and model property
     * if array specified
     * relation key specify relation attribute
     * relationId key specify custom foreign key
     * model key if set to false - ignore this relation property
     * @param array $attributes
     */
    protected function saveRelations($attributes) {
        foreach($attributes as $attribute => $attrValue) {
            if(count($this->$attribute)) {
                if(!is_array($attrValue)) {
                    $relation = $attrValue;
                    $modelAttr = true;
                    $relationAttr = 'relationId';
                } else {
                    $relation = $attrValue['relation'];
                    $modelAttr = $attrValue['modelAttr'];
                    $relationAttr = $attrValue['relationAttr'];
                }
                /* @var CActiveRelation $activeRelation */
                $activeRelation = $this->getActiveRelation($relation);
                $relModelName = $activeRelation->className;

                $relModel = new $relModelName($relation);
                foreach($this->$attribute as $data) {
                    $relModelId = (int)$data['id'];
                    // instance of relation model
                    $rm = $relModel->findByPk($relModelId);
                    if(!$rm) {
                        $rm = clone $relModel;
                        if($modelAttr)
                            $rm->model = get_class($this);
                        $rm->$relationAttr = $this->getPrimaryKey();
                    }
                    $rm->setAttributes($data);
                    $rm->save();
                }
            }
        }
    }

    /**
     * @param string $attr
     * @return bool
     * check if image with property $attr exists in file system
     */
    public function imageExists($attr = 'image') {
        $path = $this->getAttributePath($attr);
        return $this->$attr && file_exists($path);
    }

    /**
     * @param string $attr
     * @param null $size if crop or resize image
     * @param string $mode possible scaleAndCrop, scale, crop
     * @return string
     * return image url for property $attr
     */
    public function getImage($attr = 'image', $size = null, $mode = 'scaleAndCrop') {
        if($this->$attr) {
            if($size) {
                return Yii::app()->image->createUrl($this->getAttributePath($attr), $size, $mode);
            }
            return $this->getFileUrl($attr);
        } else {
            return Yii::app()->params->noimageUrl;
        }
    }

    /**
     * @return string
     * return property $title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return string
     * return property $text
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @return string
     * return property $content
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @return string
     * return property $description
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return string
     * get @seoTitle property or @title property
     * and return combined meta tag title value
     */
    public function getSeoTitle() {
        if ($this->seoTitle) {
            return $this->seoTitle;
        } else {
            return $this->title;
        }
    }

    /**
     * @return string
     * return property $seoDescription
     */
    public function getSeoDescription() {
        return $this->seoDescription;
    }

    /**
     * @return string
     * return property $seoKeywords
     */
    public function getSeoKeywords() {
        return $this->seoKeywords;
    }

    /**
     * after save processes
     */
    protected function afterSave() {
        parent::beforeSave();
        // save translation fields
        $fields = array_keys($this->translatedFields);
        if(count($fields) && !empty($this->translations)) {
            $languages = array_keys(Yii::app()->params['languages']);

            // remove all previous fields
            Translate::model()->deleteAllByAttributes(array(
                'model' => get_class($this),
                'relationId' => $this->id,
            ));

            foreach($languages as $lang) {
                foreach($fields as $field) {
                    if(isset($this->translations[$lang][$field])) {
                        $translate = new Translate();
                        $translate->relationId = $this->id;
                        $translate->model = get_class($this);
                        $translate->key = $field;
                        $translate->language = $lang;
                        $translate->value = $this->translations[$lang][$field];
                        $translate->save();
                    }
                }
            }
        }
    }

    /**
     * get localized model property value
     * @param $attribute
     * @param $lang
     * @return mixed|null
     */
    public function getLangAttribute($attribute, $lang) {
        /** @var Translate $model */
        $model = Translate::model()->findByAttributes(array(
            'model' => get_class($this),
            'relationId' => $this->id,
            'key' => $attribute,
            'language' => $lang,
        ));
        if($model)
            return $model->value;
        return null;
    }

    public function __get($name)
    {
        $lang = Yii::app()->language;
        if (in_array($name,array_keys($this->translatedFields))){
            if (!isset($this->translations[$lang][$name])){
                $this->translations[$lang][$name] = $this->getLangAttribute($name,$lang);
            }
            return $this->translations[$lang][$name];
        }
        return parent::__get($name);
    }

}