<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 13:04
 */

class WebUser extends CWebUser {

    private $_model = null;

    function getRole() {
        if($user = $this->getModel()){
            return $user->role;
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

    /**
     * try to get property from $user ActiveRecord object
     * @param string $name
     * @throws CException
     * @return mixed
     */
    public function __get($name) {
        try {
            $res = parent::__get($name);
        } catch(CException $webUserException) {
            if(!$this->isGuest) {
                try {
                    $user = $this->_getUser();
                    $res = $user->$name;
                } catch(CException $arUserException) {
                    throw new CException($arUserException->getMessage());
                }
            } else {
                throw new CException($webUserException->getMessage());
            }
        }
        return $res;
    }

    /**
     * get User instance
     * @return User
     */
    private function _getUser() {
        if(!$this->_model)
            //$this->_user = User::model()->cache(Yii::app()->params->dbCacheDuration)->findByPk($this->id);
            $this->_model = User::model()->findByPk($this->id);
        return $this->_model;
    }


    /**
     * try to get user method from ActiveRecord object
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function __call($name, $parameters) {
        if(!$this->isGuest) {
            $user = $this->_getUser();
            try {
                return call_user_func_array(array($user, $name), $parameters);
            } catch(CException $e) {
                //throw new CException($e->getMessage());
            }
        }
        return parent::__call($name, $parameters);
    }
}