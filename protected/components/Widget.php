<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 04.07.13 Time: 1:35
 */

class Widget extends CWidget {

    /**
     * cached view paths for non theme files
     * @var array
     */
    private static $_viewPaths = array();

    /**
     * cached view paths for theme files
     * @var array
     */
    private static $_themeViewPaths = array();

    /**
     * override parent
     * @param bool $checkTheme
     * @return string
     */
    public function getViewPath($checkTheme=false)
    {
        $className=get_class($this);
        if($checkTheme && isset(self::$_themeViewPaths[$className]))
            return self::$_themeViewPaths[$className];
        elseif(isset(self::$_viewPaths[$className]))
            return self::$_viewPaths[$className];
        else
        {
            if($checkTheme && ($theme=Yii::app()->getTheme())!==null)
            {
                $path=$theme->getViewPath().DIRECTORY_SEPARATOR;
                if(($module=$this->controller->getModule())!==null)
                    if(is_dir($path.DIRECTORY_SEPARATOR.$module->getId()))
                        $path .= $module->getId().DIRECTORY_SEPARATOR;
                if(is_dir($path.'widgets'))
                    $path .= 'widgets';
                if(is_dir($path))
                    return self::$_themeViewPaths[$className]=$path;
            }
            $path=$basePath=Yii::app()->getViewPath();
            if(($module=$this->controller->getModule())!==null)
                $path=$module->getViewPath();
            $path .= DIRECTORY_SEPARATOR;
            if(is_dir($path.'widgets'))
                return self::$_viewPaths[$className]=$path.'widgets';

            $class=new ReflectionClass($className);
            return self::$_viewPaths[$className]=dirname($class->getFileName()).DIRECTORY_SEPARATOR.'views';
        }
    }

}