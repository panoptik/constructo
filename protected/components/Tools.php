<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 16.05.13 Time: 0:40
 */

class Tools {

    /**
     * generate crypt hash
     * @param $str
     * @return string
     */
    static public function crypt($str) {
        $salt = isset(Yii::app()->params['salt']) ? Yii::app()->params['salt'] : '';
        if(function_exists('sha1') && function_exists('md5')) {
            $hash = sha1($salt.md5($str.$salt).$str);
        } elseif(function_exists('sha1')) {
            $hash = sha1($salt.sha1($str.$salt).$str);
        } elseif(function_exists('md5')) {
            $hash = md5($salt.md5($str.$salt).$str);
        } else {
            $hash = crypt($str, $salt);
        }
        return $hash;
    }

    /**
     * @param $path
     * @param $fileName
     * @return string
     * convert file system path to web accessible url
     */
    static public function pathToUrl($path, $fileName) {
        $ds = DIRECTORY_SEPARATOR;
        return Yii::app()->baseUrl . str_replace(array('webroot.','.'), $ds, $path) . $ds. $fileName;
    }
    
    static public function translitIt($str,$spec_crop = true,$to_lower = true){       
               
        $tr = array(
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
            "Д"=>"D","Е"=>"E","Ё"=>"YO","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
            "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
            "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
        );
        
        $str = str_replace(array_keys($tr),array_values($tr),$str);
        
        if($spec_crop === true){
            $str = preg_replace('/[^а-яА-Яa-zA-ZёЁ\d\-_]+/','_',$str);
        }
        
        if($to_lower === true){
            $str = mb_strtolower($str,'utf-8');
        }
        
        $str = preg_replace('/[_«»\'"]+/','_',$str);
        $str = trim($str,'_');
        
        return $str;
    }

    static public function uniqueId() {
        return substr(md5(rand(0,time())),0,10);
    }

    static public function encodeStringToUnicode($s) {
        $s = iconv('UTF-8', 'UCS-4LE', $s);
        $r = '';
        foreach(str_split($s, 4) as $chr) {
            $arr = unpack('Vcode', $chr);
            $r .= '\\u' . $arr['code'] . '  ';
        }

        return $r;
    }

    /**
     * send email
     * @param $toAddress
     * @param $subject
     * @param $body
     * @param string $fromEmail
     * @param string $fromName
     * @param bool $isHtml
     * @return null|array containing the unique identifier for this message and a separate request id.
     *         Returns false if the provided message is missing any required fields.
     */
    static public function sendEmail($toAddress, $subject, $body, $fromEmail = 'dev@grandiz.com', $fromName = 'Deliqat', $isHtml = true)
    {
        /** @var ESimpleEmailService $ses */
        $ses = Yii::app()->ses;
        $ses->enableVerifyHost(false);
        $ses->enableVerifyPeer(false);
        /** @var ESES $ses */
        return $ses->email()
            ->addTo($toAddress)
            ->setFrom($fromName . ' <' . $fromEmail . '>')
            ->setSubject($subject)
            ->setMessageFromString($body, $body) //Pass a string body and html body
            ->addReplyTo($fromName . ' <' . $fromEmail . '>')
            ->setSubjectCharset('UTF-8')
            ->setMessageCharset('UTF-8')
            ->send();
    }

}