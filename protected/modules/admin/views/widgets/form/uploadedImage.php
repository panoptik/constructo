<div data-id="<?=$id;?>" data-attribute="<?=$attribute;?>" data-modelname="<?=$currentModelClass;?>" data-prefix="<?=$this->prefix;?>" class="uploadedItem">
    <img src="<?=$imgUrl;?>">
    <a class="close" href="javascript:{}">&times;</a>
    <?=CHtml::hiddenField($this->prefix.'['.$attribute.']'.$this->getMultipleIndex(), $value, array('id'=>false, 'class'=>'fileName')); ?>
</div>