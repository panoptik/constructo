<div class="row">
    <?php echo $form->labelEx($model, $attribute); ?>
    <?php echo $form->textArea($model, $attribute, $htmlOptions); ?>
    <?php echo $form->error($model, $attribute); ?>
</div>