<div class="row">
    <?php echo $form->labelEx($model, $attribute); ?>
    <? $this->widget('ImperaviRedactorWidget', array(
        'model' => $model,
        'attribute' => $attribute,
    )); ?>
    <?php echo $form->error($model, $attribute); ?>
</div>