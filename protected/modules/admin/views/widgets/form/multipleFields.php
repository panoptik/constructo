<div class="row multipleFields" data-modelname="<?=$modelname;?>" data-relation="<?=$this->relationAttribute;?>" data-label="<?=$this->attributeLabel;?>" data-attribute="<?=$this->attribute;?>" data-field="<?=$this->fieldAttribute;?>" data-parent="<?=$this->parentIndex;?>" data-prefix="<?=$parentPrefix;?>" id="<?=$this->uniqueId;?>">
    <?=CHtml::label($this->attributeLabel,'');?>
    <a href="javascript:{}" class="add roundBtn <?=$this->uniqueId;?>">+</a>
    <? foreach($models as $index => $model): ?>
        <div class="fieldItem <?=$this->uniqueId;?>" data-index="<?=$index;?>" data-id="<?=$model->id;?>">
            <?=CHtml::textField("{$parentPrefix}[{$this->fieldAttribute}][{$index}][{$this->attribute}]",$model->{$this->attribute},array('id'=>false)); ?>
            <?=CHtml::hiddenField("{$parentPrefix}[{$this->fieldAttribute}][{$index}][id]",$model->id); ?>
            <a href="javascript:{}" class="remove roundBtn">-</a>
        </div>
    <? endforeach; ?>
</div>

