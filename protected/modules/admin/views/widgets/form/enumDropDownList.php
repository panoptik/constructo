<div class="row">
    <?php echo $form->labelEx($model, $attribute); ?>
    <?= Html::enumDropDownList($model, $attribute, $htmlOptions); ?>
    <?php echo $form->error($model, $attribute); ?>
</div>