<div class="row">
    <?php echo $form->labelEx($model, $attribute); ?>
    <?php echo $form->dropDownList($model, $attribute, $data, $htmlOptions); ?>
    <?php echo $form->error($model, $attribute); ?>
</div>