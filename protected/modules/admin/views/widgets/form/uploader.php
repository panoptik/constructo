<div class="row">
    <?php echo $form->labelEx($model, '['.$this->uniqueId.']'.$attribute); ?>
    <?php $this->widget('AjaxUpload', array(
        'model' => $model,
        'attribute' => $attribute,
        'multiple' => $this->multiple,
        'uniqueId' => $this->uniqueId,
        'prefix' => $this->prefix,
    )); ?>
    <? if($this->multiple): ?>
        <div id="<?=CHtml::activeId($model, $this->relation);?>_files">
            <? foreach($model->{$this->relation} as $relModel) {
                $this->widget('WUploadedImage', array(
                    'model' => $model,
                    'attribute' => $attribute,
                    'relation' => $this->relation,
                    'relationAttribute' => $this->relationAttribute,
                    'multiple' => $this->multiple,
                    'prefix' => $this->prefix,
                ));
            } ?>
        </div>
    <? else: ?>
        <div id="<?=CHtml::activeId($model, $attribute);?>_files">
            <?php if($model->$attribute) {
                $this->widget('WUploadedImage', array(
                    'model' => $model,
                    'attribute' => $attribute,
                    'multiple' => $this->multiple,
                    'prefix' => $this->prefix,
                ));
            } ?>
        </div>
    <? endif; ?>
</div>