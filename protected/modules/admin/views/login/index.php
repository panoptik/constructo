<!--login modal-->
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="text-center">Login</h1>
            </div>
            <div class="modal-body" style="height: 200px;">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                    'htmlOptions'=>array(
                        'class'=> 'form col-md-12 center-block',
                    ),
                )); ?>
                    <div class="form-group">
                        <?= $form->textField($model,'email', array('class'=>'form-control input-lg','placeholder'=>'Email')); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->passwordField($model,'password', array('class'=>'form-control input-lg','placeholder'=>'Password')); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo CHtml::submitButton('Sign In',array('class'=>'btn btn-primary btn-lg btn-block')); ?>
                        <span class="pull-right"><a href="<?=$this->createUrl('/auth/passwordRecovery')?>">Forgot password</a></span>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
</div>