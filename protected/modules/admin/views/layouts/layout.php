<?php /** @var string $content */ ?>
<?php /** @var CClientScript $cs */ ?>
<?php $cs = Yii::app()->clientScript; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php Yii::app()->bootstrap->register(); ?>

    <?php $cs->registerCssFile($this->assetUrl . '/css/sb-admin.css'); ?>
    <?php $cs->registerCssFile($this->assetUrl . '/css/font-awesome.min.css'); ?>

    <title>Admin Interface</title>

</head>

<body>

<div id="wrapper">
<?=$content;?>
</div><!-- /#wrapper -->

</body>
</html>

