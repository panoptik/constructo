<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Bogdan
 * Date: 31.07.13
 * Time: 14:00
  */

/**
 * upload file to tmp directory
 */
class AUpload extends CAction {

    /**
     * @var string
     */
    public $modelName;

    public function run() {
        $controller = $this->getController();

        /* @var CHttpRequest $request */
        $request = Yii::app()->request;
        Yii::import("admin.extensions.uploader.qqFileUploader");

        $folder = Yii::getPathOfAlias(Yii::app()->params->tmpUploadFolder).'/';
        if(!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0777);
        }


        $allowedExtensions = array(
            //images
            "jpg","jpeg","gif","png",
            //sounds
            "mp3","wav","aac",
            //documents
            "txt","pdf","doc","docx","xls","xlsx","csv","psd",
            //archives
            "zip","rar","7z","tar","gz");

        // create new temporary name with tmp_ prefix to identify file as new
        $tmp_filename = 'tmp_'.time().'_'.substr(md5(rand(0, time())),0,12);
        $sizeLimit = Yii::app()->params->uploadSizeLimit;// maximum file size in bytes
        // create uploader and set new file name
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit, $tmp_filename);
        $result = $uploader->handleUpload($folder);

        $attribute = $request->getQuery('attribute');
        $multiple = $request->getQuery('multiple');
        $relation = $request->getQuery('relation');
        $relationAttribute = $request->getQuery('relationAttribute');
        $modelName = $request->getQuery('model');
        $prefix = $request->getQuery('prefix');

        if($modelName)
            $this->modelName = $modelName;

        $model = CActiveRecord::model($this->modelName);
        $model->$attribute = $result['filename'];

        $imgUrl = Tools::pathToUrl(Yii::app()->params->tmpUploadFolder, $result['filename']);

        // render uploaded item to show it ufter upload
        $html = $controller->widget('WUploadedImage', array(
            'imgUrl' => $imgUrl,
            'model' => $model,
            'attribute' => $attribute,
            'multiple' => $multiple,
            'relation' => $relation,
            'relationAttribute' => $relationAttribute,
            'prefix' => $prefix,
        ), true);

        $result['html'] = $html;
        $result['original'] = $request->getQuery('qqfile');

        echo CJSON::encode($result);
    }

}