<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Bogdan
 * Date: 31.07.13
 * Time: 17:09
  */

/**
 * delete model files (image etc.)
 */
class ADeleteFile extends CAction {

    /**
     * @var string
     */
    public $modelName;

    public function run() {
        /* @var CHttpRequest $request */
        $request = Yii::app()->request;
        $fileName = $request->getPost('fileName');
        $modelName = $request->getPost('modelName');

        $id = $request->getPost('id');
        $attribute = $request->getPost('attribute');


        /* @var ActiveRecord $model */
        $model = CActiveRecord::model($modelName)->findByPk($id);
        // if it is existing model attribute value - delete file, and clear model property
        if($model) {
            if($modelName != $this->modelName) {
                $path = Yii::getPathOfAlias($model->getAttributeDirAlias($attribute));
                $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
                if(file_exists($filePath)) {
                    unlink($filePath);
                }
                $model->delete();
            } else {
                $model->$attribute = '';
                $model->save(false);
                $path = Yii::getPathOfAlias($model->getAttributeDirAlias($attribute));
                $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
                if(file_exists($filePath)) {
                    unlink($filePath);
                }
            }
            // seems it is new file - only delete it from filesystem
        } else {
            $path = Yii::getPathOfAlias(Yii::app()->params->tmpUploadFolder);
            $filePath = $path . $fileName;
            if(file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }

}