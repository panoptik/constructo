<?php
/**
 * Date: 25.03.14
 * Time: 11:01
  */

class AdminCrudController extends AdminController {

    /**
     * used for creating link to current controller
     * @var string
     */
    protected $name;

    /**
     * used for main crud operations with single model in controller
     * @var string
     */
    protected $modelName;

    /**
     * define title for breadcrumbs and other views
     * @var string
     */
    protected $title;

    /**
     * show addButton in index form
     * default set to true
     * @var bool
     */
    public $addButton = true;

    /**
     * use default index view if set to true
     * default set to false
     * @var bool
     */
    public $useDefaultIndexView = false;

    /**
     * default index view for crud operations
     * @var string
     */
    public $defaultIndexView = '/layouts/crudIndex';

    /**
     * data for generating model grid view
     * @var array
     */
    protected $gridData = array(
        'columns' => array(
            'id' => array(
                'name' => 'id',
                'htmlOptions'=>array(
                    'style'=>'width: 60px;'
                ),
            ),
            'name' => array(
                'name' => 'name',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, $this->grid->owner->createUrl("update",array("id"=>$data->id)),array("class"=>"update"))',
            ),
        ),
    );

    /**
     * use default form view if set to true
     * default set to false
     * @var bool
     */
    public $useDefaultFormView = false;

    /**
     * default form view for crud operations
     * @var string
     */
    public $defaultFormView = '/layouts/crudForm';

    /**
     * used when defaultFormView set to true
     * form fields (model attributes) to render in add/update form
     * key -> attribute name
     * value -> array with mandatory @string item widget Class (key "widgetClass")
     * to render attribute form field
     * @var array
     */
    public $formFields = array();

    /**
     * when set to true, enable ability to change form in popup window
     * and save forms using ajax
     * default set to false
     * @var bool
     */
    public $enableAjaxCrud = false;

    /**
     * attributes of model which will be name of uploaded files
     * @var array
     */
    public $modelFileAttributes = array('image');

    /**
     * initialize method
     * must be extended in controller
     * to set default crud values
     */
    public function init() {
        parent::init();
        $this->name = $this->id;

        if(Yii::app()->request->isAjaxRequest) {
            $clientScript = Yii::app()->getClientScript();
            $clientScript->scriptMap=array(
                "jquery.js" => false,
                "jquery.min.js" => false,
                "jquery-ui.min.js" => false,
                "jquery-ui.min.css" => false,
            );
        }
    }

    /**
     * include external actions
     * @return array
     */
    public function actions() {
        return array(
            'upload'=>array(
                'class'=>'admin.components.actions.AUpload',
                'modelName'=>$this->modelName,
            ),
            'deleteFile'=>array(
                'class'=>'admin.components.actions.ADeleteFile',
                'modelName'=>$this->modelName,
            ),
        );
    }

    /**
     * filters settings
     * @return array
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    /**
     * access control settings
     * used RBAC
     * @return array
     */
    public function accessRules() {
        return array(
            array(
                'allow',
                'roles' => array('admin'),
            ),
            array('allow',
                'actions'=>array('index', 'view', 'translit', 'getWysiwygImages', ),
                'roles'=>array('viewAction'),
            ),
            array('allow',
                'actions'=>array('delete'),
                'roles'=>array('deleteAction'),
            ),
            array('allow',
                'actions'=>array('add'),
                'roles'=>array('createAction'),
            ),
            array('allow',
                'actions'=>array('update', 'addRelationField', 'removeRelationField', 'upload', 'deleteFile', 'uploadWysiwygImages'),
                'roles'=>array('updateAction'),
            ),
            array('deny'),
        );
    }

    /**
     * show list of items
     */
    public function actionIndex() {
        $this->breadcrumbs[] = $this->title;

        $modelName = $this->modelName;
        if(!$modelName)
            throw new CException('You must specify correct modelName');
        /** @var ActiveRecord $model */
        $model = new $modelName('search');
        $model->unsetAttributes();

        $request = Yii::app()->request;
        $pageSize = (int)$request->getQuery('pageSize');
        if ($pageSize) {
            Yii::app()->user->setState('pageSize', $pageSize);
            unset($_GET['pageSize']);
        }
        $searchData = $request->getQuery($this->modelName);
        if($searchData) {
            $model->attributes = $searchData;
        }

        $this->setGridData($model);

        $data = array(
            'model' => $model,
        );
        if($this->useDefaultIndexView)
            $view = $this->defaultIndexView;
        else
            $view = 'index';

        $this->render($view, $data);
    }

    /**
     * set CGridView / GridView data to render
     * @param $model
     */
    protected function setGridData($model) {
        /* @var CActiveDataProvider $dataProvider */
        $dataProvider = $model->search();
        $dataProvider->pagination->pageSize = Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']);
        $this->gridData = CMap::mergeArray($this->gridData, array(
            'id' => $this->name.'-grid',
            'dataProvider' => $dataProvider,
            'filter' => $model,
        ));
    }

    // behaviours

    /**
     * before save behaviour when add new item
     * @param ActiveRecord $model
     */
    protected function beforeAdd($model) {}

    /**
     * before update behaviour when update item
     * @param ActiveRecord $model
     */
    protected function beforeUpdate($model) {}

    /**
     * before save both add and update behaviour
     * @param ActiveRecord $model
     */
    protected function beforeSaveInit($model) {}

    /**
     * before validate behaviour
     * @param ActiveRecord $model
     */
    protected function beforeValidate($model) {}

    /**
     * after save behaviour
     * @param ActiveRecord $model
     */
    protected function afterSave($model) {}

    /**
     * before render when save item behaviour
     * @param ActiveRecord $model
     */
    protected function beforeRenderSave($model) {}

    /**
     * add new item
     */
    public function actionAdd() {
        $modelName = $this->modelName;
        $model = new $modelName('add');

        // call behaviour
        $this->beforeAdd($model);

        $this->_save($model);
    }

    /**
     * edit existing item
     */
    public function actionUpdate($id) {
        $modelName = $this->modelName;
        $model = $modelName::model()->findByPk($id);

        if(!$model)
            throw new CHttpException(404);

        $model->scenario = 'edit';

        // call behaviour
        $this->beforeUpdate($model);

        $this->_save($model);
    }

    /**
     * render add/update form
     * and save model
     * @param ActiveRecord $model
     */
    protected function _save($model) {
        // call behaviour
        $this->beforeSaveInit($model);

        $ajax = Yii::app()->request->isAjaxRequest;
        $controllerHomeUrl = $this->createUrl($this->getId().'/index');
        $this->breadcrumbs[$this->title] = $controllerHomeUrl;
        $this->breadcrumbs[] = $model->isNewRecord ?
            Yii::t('f', 'add') : Yii::t('f', 'edit');
        if(isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            // before validate behaviour
            $this->beforeValidate($model);
            if($model->validate(null, false) && !$model->hasErrors()) {
                $model->save(false);

                // after save behaviour
                $this->afterSave($model);

                $this->_saveFiles($model);

                Yii::app()->user->setFlash('success',  Yii::t('t','Successfully saved'));

                if(isset($_POST['apply']) && $_POST['apply']) {
                    if($ajax) {
                        echo 'apply';
                        Yii::app()->end();
                    } else {
                        $this->redirect($this->createUrl($this->getId().'/update', array('id'=>$model->id)));
                    }
                } else {
                    if($ajax) {
                        echo 'success';
                        Yii::app()->end();
                    } else {
                        $this->redirect($controllerHomeUrl);
                    }
                }
            }
        }

        // call behaviour
        $this->beforeRenderSave($model);

        $data = array(
            'model' => $model,
        );

        if($this->useDefaultFormView)
            $view = $this->defaultFormView;
        else
            $view = 'form';

        if($ajax) {
            header('Content-Type: text/html; charset=utf-8');
            $this->renderPartial($view, $data, false, true);
        } else
            $this->render($view, $data);
    }

    /**
     * delete item
     */
    public function actionDelete($id) {
        $modelName = $this->modelName;
        $model = $modelName::model()->FindByPk($id);

        if(!$model)
            throw new CHttpException(404);

        if(Yii::app()->request->requestType == 'POST') {
            $model->delete();
        }
    }

    /**
     * save model files (images etc.)
     * as own properties or relations
     * @param $model
     */
    protected function _saveFiles($model) {
        /* @var CHttpRequest $request */
        $request = Yii::app()->request;
        $attributes = $this->modelFileAttributes;
        $postData = $request->getPost($this->modelName);
        foreach($attributes as $attribute) {
            $file = isset($postData[$attribute]) ? $postData[$attribute] : null;
            $this->_saveFile($model, $attribute, $file, $model->getPrimaryKey());
        }
    }

    /**
     * save single file
     * @param ActiveRecord $model
     * @param string $attribute
     * @param string $file
     * @param integer $id
     * @return string filename
     */
    public function _saveFile($model, $attribute, $file, $id = null) {
        $tmpUploadFolder = Yii::getPathOfAlias(Yii::app()->params->tmpUploadFolder);
        $newFileName = $file;
        // check if it is new uploaded file with tmp_ prefix
        if($file && substr($file, 0, 3) === 'tmp') {
            $fullFilePath = $tmpUploadFolder . DIRECTORY_SEPARATOR . $file;
            if(file_exists($fullFilePath)) {
                $ext = strtolower(pathinfo($fullFilePath, PATHINFO_EXTENSION));
                if(!$id)
                    $id = $model->getPrimaryKey();
                $path = Yii::getPathOfAlias($model->getAttributeDirAlias($attribute));
                $newFileName = $attribute . '_' . $id . '_' . time() . '_' . substr(md5(rand(0,time())),0,10) . '.' . $ext;
                // directly replace file into storage place
                $replaceRes = rename($fullFilePath, $path . DIRECTORY_SEPARATOR . $newFileName);
                if($replaceRes) {
                    $model->$attribute = $newFileName;
                    $model->save(false);
                }
            }
        }
        return $newFileName;
    }

    /**
     * add relation field to form
     */
    public function actionAddRelationField() {
        /* @var CHttpRequest $request */
        $request = Yii::app()->request;
        $relation = $request->getPost('relation');
        $attribute = $request->getPost('attribute');
        $label = $request->getPost('label');
        $startIndex = (int)$request->getPost('startIndex');
        $parentIndex = (int)$request->getPost('parentIndex');
        $modelname = $request->getPost('modelname');
        $field = $request->getPost('field');

        if(!$modelname && $this->modelName)
            $modelname = $this->modelName;

        $model = new $modelname();

        $this->widget('WMultipleFields', array(
            'relationAttribute' => $relation,
            'attribute' => $attribute,
            'attributeLabel' => $label,
            'model' => $model,
            'startIndex' => $startIndex,
            'parentIndex' => $parentIndex,
            'fieldAttribute' => $field,
        ));
    }

    /**
     * remove relation field from form
     */
    public function actionRemoveRelationField() {
        /* @var CHttpRequest $request */
        $request = Yii::app()->request;
        $relation = $request->getPost('relation');
        $modelname = $request->getPost('modelname');
        $id = (int)$request->getPost('id');

        if(!$modelname && $this->modelName)
            $modelname = $this->modelName;

        /* @var CActiveRecord $model */
        $model = new $modelname();

        /* @var CActiveRelation $activeRelation */
        $activeRelation = $model->getActiveRelation($relation);
        $relationClassName = $activeRelation->className;

        $relationModel = $relationClassName::model()->findByPk($id);
        if($relationModel) {
            $relationModel->delete();
        }
    }

    /**
     * render form fields
     * @param array $formFields
     * @param CActiveForm $form
     * @param ActiveRecord $model
     */
    protected function renderFields($formFields, $form, $model) {
        foreach($formFields as $attribute => $options) {
            $this->widget($options['widgetClass'], array(
                'form'=>$form,
                'model'=>$model,
                'attribute'=>$attribute,
                'data'=>isset($options['data']) ? $options['data'] : null,
                'htmlOptions' => isset($options['htmlOptions']) ? $options['htmlOptions'] : array(),
            ));

        }
    }

    /**
     * upload file from wysiwyg redactor
     * @throws CException
     */
    public function actionUploadWysiwygImages() {
        if(!empty($_FILES['file']['name'])) {
            $fileA = $_FILES['file'];
            $fileName = Tools::translitIt($fileA['name']);
            $uploadDir = Yii::getPathOfAlias(Yii::app()->params->wysiwygImageFolder) . DIRECTORY_SEPARATOR;
            $filePath = $uploadDir . $fileName;
            if(move_uploaded_file($fileA['tmp_name'], $filePath)) {
                /* @var CImage $image */
                $image = Yii::app()->image;
                $path = $image->createPath($filePath, '50x50');

                rename($path, $uploadDir . 'thumb_' . $fileName);

                $json = array(
                    'filelink' => Tools::pathToUrl(Yii::app()->params->wysiwygImageFolder, $fileName),
                );
                echo CJSON::encode($json);
                return;
            }
            throw new CException('Cannot upload file "' . $fileA['tmp_name'] . '" into "' . $filePath . '"');
        }
        throw new CException('No file got');
    }

    public function actionGetWysiwygImages() {
        $folderAlias = Yii::app()->params->wysiwygImageFolder;

        $uploadDir = Yii::getPathOfAlias($folderAlias) . DIRECTORY_SEPARATOR;
        $dirItems = scandir($uploadDir);
        $json = array();

        foreach($dirItems as $dirItem) {
            if(strpos($dirItem, 'thumb_') === 0) {
                $imageName = substr($dirItem, 6);
                $item = array(
                    'thumb' => Tools::pathToUrl($folderAlias, $dirItem),
                    'image' => Tools::pathToUrl($folderAlias, $imageName),
                    'folder' => 'img',
                    'title' => $imageName
                );
                $json[] = $item;
            }
        }
        echo CJSON::encode($json);
    }

    /**
     * view info about selected item
     * @param $id
     * @throws CHttpException
     */
    public function actionView($id) {
        $model = CActiveRecord::model($this->modelName)->findByPk($id);
        if(!$model)
            throw new CHttpException(404);

        $data = array(
            'model' => $model,
        );

        $controllerHomeUrl = $this->createUrl($this->getId().'/index');
        $this->breadcrumbs[$this->title] = $controllerHomeUrl;
        $this->breadcrumbs[] = Yii::t('f', 'View');

        $this->render('view', $data);
    }

    /**
     * translit title text for url alias
     */
    public function actionTranslit() {
        $text = Yii::app()->request->getParam('text');
        echo Tools::translitIt($text);
    }

} 