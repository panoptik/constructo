<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.05.13 Time: 23:49
 */

class AdminController extends Controller {

    public $layout = '/layouts/main';

    /**
     * page header title
     * @var string
     */
    protected $title = '';

    /**
     * used in breadcrumbs widget to show admin home url as home url
     * @var string
     */
    public $homeUrl="/admin/";

    public $assetUrl = '';


    /**
     * filters settings
     * @return array
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    /**
     * access control settings
     * used RBAC
     * @return array
     */
    public function accessRules() {
        return array(
            array(
                'allow',
                'roles' => array('admin'),
            ),
            array(
                'deny',
            ),
        );
    }

}