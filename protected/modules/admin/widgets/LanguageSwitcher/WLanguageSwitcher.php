<?php
/**
 * Date: 28.03.14
 * Time: 12:20
  */

class WLanguageSwitcher extends Widget
{
    public function run()
    {
        $assetPath = __DIR__ . DIRECTORY_SEPARATOR . 'assets';
        /** @var CAssetManager $assetManager */
        $assetManager = Yii::app()->assetManager;
        $assetUrl = $assetManager->publish($assetPath, false, -1, YII_DEBUG);

        /** @var CClientScript $cs */
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($assetUrl . '/css/langSwitcher.css');

        $currentUrl = ltrim(Yii::app()->request->url, '/');
        $links = array();
        //foreach (DMultilangHelper::suffixList() as $suffix => $name){
        //    $url = '/' . ($suffix ? trim($suffix, '_') . '/' : '') . $currentUrl;
        //    $links[] = CHtml::tag('li', array('class'=>$suffix), CHtml::link($name, $url));
        //}

        foreach (Yii::app()->params['languages'] as $code => $name){
            $active = (Yii::app()->language == $code) ? 'active' : '';
            $url = '/' . ($code != Yii::app()->params['defaultLanguage'] ?
                    ($code ? trim($code, '_') . '/' : '') : '') . $currentUrl;
            $image = CHtml::image($assetUrl . '/img/' . $code . '.png');
            $link = CHtml::link($image . $name, $url);
            $links[] = CHtml::tag('li', array('class'=>$code . ' ' . $active), $link);
        }

        echo CHtml::tag('ul', array('class'=>'language-switcher-container'), implode("", $links));
    }
}