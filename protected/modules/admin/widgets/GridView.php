<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 13.06.13 Time: 2:12
 */

class GridView extends CGridView {

    public $addCButtonColumnRow = true;

    private $defaultButtonColumns = array(
        'template' => '{update}{delete}',
        'htmlOptions' => array(
            'style'=>'width: 50px; text-align: center;',
        ),
        'buttons'=>array(
            'update'=>array(
                'visible' => 'Yii::app()->user->checkAccess("updateAction")',
            ),
            'delete'=>array(
                'visible' => 'Yii::app()->user->checkAccess("deleteAction")',
            ),
            'view'=>array(
                'visible' => 'Yii::app()->user->checkAccess("viewAction")',
            ),
        ),
    );

    public $overwriteButtonColumns = false;
    public $buttonColumns = array();

    protected function initColumns()
    {
        if($this->columns===array())
        {
            if($this->dataProvider instanceof CActiveDataProvider)
                $this->columns=$this->dataProvider->model->attributeNames();
            elseif($this->dataProvider instanceof IDataProvider)
            {
                // use the keys of the first row of data as the default columns
                $data=$this->dataProvider->getData();
                if(isset($data[0]) && is_array($data[0]))
                    $this->columns=array_keys($data[0]);
            }
        }
        $id=$this->getId();

        /*
         * check last column and if it ButtonColumn
         */
        if($this->addCButtonColumnRow) {
            $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
            $buttonColumns = array(
                'class' => 'ButtonColumn',
                'filter'=> CHtml::dropDownList(
                        'pageSize',
                        $pageSize,array(10=>10,20=>20,50=>50,100=>100),
                        array(
                            'class' => 'form-control',
                            'onchange'=>"$.fn.yiiGridView.update('" . $this->id . "',{ data:{pageSize: $(this).val() }})",
                        )
                    ),
            );
            if($this->overwriteButtonColumns) {
                $this->columns['columnButtons'] = array_merge($buttonColumns, $this->buttonColumns);
            } else {
                $buttonColumns = CMap::mergeArray($buttonColumns, $this->defaultButtonColumns);
                $this->columns['columnButtons'] = CMap::mergeArray($buttonColumns, $this->buttonColumns);
            }
        }
        foreach($this->columns as $i=>$column)
        {
            if(is_string($column))
                $column=$this->createDataColumn($column);
            else
            {
                if(!isset($column['class']))
                    $column['class']='CDataColumn';
                $column=Yii::createComponent($column, $this);
            }
            if(!$column->visible)
            {
                unset($this->columns[$i]);
                continue;
            }
            if($column->id===null)
                $column->id=$id.'_c'.$i;
            $this->columns[$i]=$column;
        }

        foreach($this->columns as $column)
            $column->init();



    }

}