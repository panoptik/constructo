<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 13:59
 */

class WDropDownList extends WFormFields {

    public function run() {
        if(isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] .= ' select';
        } else {
            $this->htmlOptions['class'] = 'select';
        }

        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'data'=>$this->data,
            'htmlOptions'=>$this->htmlOptions,
        );
        $this->render('form/dropDownList', $data);
    }

}