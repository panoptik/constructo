<?php
/**
 * Created by PhpStorm.
 * User: senseless-long-name
 * Date: 3/27/14
 * Time: 9:28 AM
 *
 * Generates visible field with target attribute
 * and invisible city,country,latitude,longitude
 */

class WLocationField extends WFormFields {

    public function run() {
        $assetPath = __DIR__ . DIRECTORY_SEPARATOR . 'assets'. DIRECTORY_SEPARATOR . get_class($this);

        /** @var CAssetManager $assetManager */
        $assetManager = Yii::app()->assetManager;
        $assetUrl = $assetManager->publish($assetPath);

        /** @var CClientScript $clientScript */
        $clientScript = Yii::app()->clientScript;
        $clientScript->registerScriptFile('http://maps.google.com/maps/api/js?sensor=true&language=en');
        $clientScript->registerScriptFile($assetUrl . '/google.js');

        $fields = array('city','country','latitude','longitude');
        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
            'hiddenFields'=>$fields,
        );
        $this->render('form/locationField', $data);
    }

} 