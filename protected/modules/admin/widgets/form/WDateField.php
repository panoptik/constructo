<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 16.06.13 Time: 19:29
 */

class WDateField extends WFormFields {

    public function run() {
        $model = $this->model;
        $attribute = $this->attribute;
        if(!$this->model->$attribute) {
            $this->model->$attribute = date('Y-m-d H:i:s');
        }

        $htmlOptions = $this->htmlOptions;
        if(!isset($htmlOptions['value'])) {
            $htmlOptions['value'] = date('Y-m-d',strtotime($model->$attribute));
        }

        $data = array(
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $attribute,
            'htmlOptions'=>$htmlOptions,
        );
        $this->render('form/dateField', $data);
    }


}