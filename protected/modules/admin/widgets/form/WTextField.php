<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 10:41
 */

class WTextField extends WFormFields {

    public function run() {
        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
        );
        $this->render('form/textField', $data);
    }

}