<?php
/**
 * Created by PhpStorm.
 * User: panoptik
 * Date: 30.10.13 Time: 6:07
 */

class WManyToMany extends WFormFields {

    /**
     * all available list items
     * @var array
     */
    private $_listItems = array();

    /**
     * current enabled list items
     * @var array
     */
    private $_currentItems = array();

    public function run() {
        $activeRelation = $this->model->getActiveRelation($this->attribute);
        $className = $activeRelation->className;
        $models = CActiveRecord::model($className)->findAll();

        $this->_listItems = CHtml::listData($models, 'id', 'title');

        foreach($this->model->{$this->attribute} as $relationItem) {
            $this->_currentItems[] = $relationItem->id;
        }

        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
            'listItems'=>$this->_listItems,
        );
        $this->render('form/manyToMany', $data);
    }

    public function isChecked($currentItem) {
        return in_array($currentItem, $this->_currentItems);
    }

} 