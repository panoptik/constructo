<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 10:42
 */

class WFormFields extends Widget
{

    /**
     * model data
     * @var CModel
     */
    public $model;

    /**
     * model attribute
     * @var string
     */
    public $attribute;

    /**
     * addition data
     * for example item list for dropDownList
     * @var
     */
    public $data;

    /**
     * form to render fields
     * @var CActiveForm
     */
    public $form;

    /**
     * addition html options for specified fields
     * @var array
     */
    public $htmlOptions = array();

    /**
     * widget container tag
     * @var string
     */
    public $containerTag = 'div';

    /**
     * widget container tag html options
     * @var array
     */
    public $containerOptions = array();

}