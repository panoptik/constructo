var GOOGLE = {
    model : '',
    field : '',

    autocomplete: function (){
        geocoder = new google.maps.Geocoder();
        var selector = $('#'+this.model+'_'+this.field);
        selector.autocomplete({
            //Определяем значение для адреса при геокодировании
            source: function(request, response) {
                geocoder.geocode( {
                    'address': request.term
                }, function(results, status) {
                    response($.map(results, function(item) {
                        return {
                            
                            label:  item.formatted_address,
                            value: item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        }
                    }));
                })
            },
            //Выполняется при выборе конкретного адреса
            select: function(event, ui) {
                geocoder.geocode({
                    'address': ui.item.value
                }, function(results, status) {
                    GOOGLE.processGeoCode(results);
                });
                
            },
            appendTo: selector.parent()
        });

    },

    autocompleteLocation: function() {
        console.log(this.model,this.field)
        geocoder = new google.maps.Geocoder();
        $('#'+this.model+'_'+this.field).autocomplete({
            //Определяем значение для адреса при геокодировании
            source: function(request, response) {
                geocoder.geocode( {
                    'address': request.term
                }, function(results, status) {
                    response($.map(results, function(item) {
                        return {
                            label:  item.formatted_address,
                            value: item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        }
                    }));
                })
            },
            //Выполняется при выборе конкретного адреса
            select: function(event, ui) {
                geocoder.geocode({
                    'address': ui.item.value
                }, function(results, status) {
                    GOOGLE.processGeoCode(results);
                });
            }
        });
    },

    autocompleteLocationFilters: function (){
        geocoder = new google.maps.Geocoder();
        $('#'+this.model+'_'+this.field).autocomplete({
            //Определяем значение для адреса при геокодировании
            source: function(request, response) {
                geocoder.geocode( {
                    'address': request.term
                }, function(results, status) {
                    response($.map(results, function(item) {
                        return {

                            label:  item.formatted_address,
                            value: item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        }
                    }));
                })
            },
            //Выполняется при выборе конкретного адреса
            select: function(event, ui) {
                geocoder.geocode({
                    'address': ui.item.value
                }, function(results, status) {
                    GOOGLE.processGeoCode(results);
                });
            }
        });

    },

    autocompleteCurrencyCountry: function() {
        geocoder = new google.maps.Geocoder();
        $('#'+this.model+'_'+this.field).autocomplete({
            //Определяем значение для адреса при геокодировании
            source: function(request, response) {
                geocoder.geocode( {
                    'address': request.term
                }, function(results, status) {
                    response($.map(results, function(item) {
                        var countryCode = '',
                            country = '';
                        var l = item.address_components.length;
                        if (item.address_components[l - 1] !== undefined) {
                            countryCode = item.address_components[l - 1].short_name;
                            country = item.address_components[l - 1].long_name;
                        }
                        if (countryCode.length != 2 && l>1){
                            countryCode = item.address_components[l - 2].short_name;
                            country = item.address_components[l - 2].long_name;
                        }
                        return {
                            label:  item.formatted_address,
                            value:  country,
                            countryCode: countryCode,
                            country: country
                        }
                    }));
                })
            },
            //Выполняется при выборе конкретного адреса
            select: function(event, ui) {
                geocoder.geocode({
                    'address': ui.item.value
                }, function(results, status) {
                    GOOGLE.processGeoCode(results);
                });
            }
        });
    },

    processGeoCode: function(results) {

        var prefix = this.model;

        var countryField = $('#'+prefix+'_country'),
            cityField = $('#'+prefix+'_city'),
            latField = $('#'+prefix+'_latitude'),
            lngField = $('#'+prefix+'_longitude'),
            countryCodeField = $('#'+prefix+'_countryCode');

        countryField.val('');
        cityField.val('');
        latField.val('');
        lngField.val('');
        countryCodeField.val('');

        var r = results[0],
            loc = r.geometry.location,
            city, country, l, countryCode;
        l = r.address_components.length;
        if (r.address_components[l - 1] !== undefined) {
            country = r.address_components[l - 1].long_name;
            countryField.val(country);
            countryCode = r.address_components[l - 1].short_name;
            countryCodeField.val(countryCode);
        }
        if (r.address_components[0] !== undefined ) {
            city = r.address_components[0].long_name;
            cityField.val(city);
        }
        if(country == '') {
            countryField.val(city);
        }
        if(countryCode == '') {
            countryCodeField.val('');
        }
        if (loc.ob !== undefined) {
            //console.log(loc.ob);
            latField.val(loc.ob);
        }
        if (loc.lat !== undefined) {
            //console.log(loc.lat());
            latField.val(loc.lat());
        }
        if (loc.pb !== undefined) {
            //console.log(loc.pb);
            lngField.val(loc.pb);
        }
        if (loc.lng !== undefined) {
            //console.log(loc.lng());
            lngField.val(loc.lng());
        }
    }

};
