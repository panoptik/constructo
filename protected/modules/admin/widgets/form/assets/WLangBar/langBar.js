$(function() {
    $("#lang-choose-bar").on('click', '.language-selector a', function() {
        var o = $(this),
            lang = o.data('lang');
        $('.language-selector a').removeClass('active');
        o.addClass('active');

        $('.language-field-container').hide();
        $('.language-field-container.'+lang).show();
    });
});