<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 10.09.13 Time: 7:28
 */

class WPlainText extends WFormFields {

    public function run() {
        $text = $this->model->{$this->attribute};
        if(!empty($this->htmlOptions['text'])) {
            $text = $this->htmlOptions['text'];
            unset($this->htmlOptions['text']);
        }

        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
            'text'=>$text,
        );
        $this->render('form/plainText', $data);
    }

}