<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 13:57
 */

class WPasswordField extends WFormFields {

    public function run() {
        $this->htmlOptions['autocomplete'] = 'off';
        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
        );
        $this->render('form/passwordField', $data);
    }

}