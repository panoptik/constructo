<?php
/**
 * Date: 26.03.14
 * Time: 18:13
  */

class WLangBar extends Widget {

    /**
     * flag if widget was already called
     * @var bool
     */
    private static $instantiated = false;

    /**
     * @var CClientScript
     */
    private $cs;

    /**
     * base assets url
     * @var string
     */
    private $assetUrl;

    public function init() {
        parent::init();

        $assetPath = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . get_class($this);

        /** @var CAssetManager $assetManager */
        $assetManager = Yii::app()->assetManager;
        $this->assetUrl = $assetManager->publish($assetPath);

        /** @var CClientScript $clientScript */
        $this->cs = Yii::app()->clientScript;
    }

    public function run() {
        // disallow call widget twice
        if(self::$instantiated)
            return;

        $this->registerCss();
        $this->registerJs();

        $languages = Yii::app()->params['languages'];
        self::$instantiated = true;
        $data = array(
            'languages' => $languages,
        );
        $this->render('form/langBar', $data);
    }

    /**
     * get flag image by code
     * @param $code
     * @return string
     */
    public function getIconUrl($code) {
        return $this->assetUrl . '/img/flags/' . $code . '.png';
    }

    /**
     * register all necessary js code
     */
    protected function registerJs() {
        $this->cs->registerScriptFile($this->assetUrl . '/langBar.js');
    }

    /**
     * register all necessary css code
     */
    protected function registerCss() {
        $this->cs->registerCssFile($this->assetUrl . '/langBar.css');

        $curLang = Yii::app()->language;
        $css = <<<CSS
            .language-field-container {
                display: none;
            }
            .language-field-container.{$curLang} {
                display: block;
            }
CSS;
        $this->cs->registerCss('show#lang', $css);
    }

}