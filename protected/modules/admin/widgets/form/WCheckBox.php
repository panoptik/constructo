<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 14:50
 */

class WCheckBox extends WFormFields {

    public function run() {
        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
            'htmlOptions'=>$this->htmlOptions,
        );
        $this->render('form/checkBox', $data);
    }

}