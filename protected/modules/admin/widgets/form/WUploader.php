<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 18:10
 */

class WUploader extends WFormFields {

    /**
     * flag if multiple uploads
     * @var bool
     */
    public $multiple = false;

    /**
     * relation name for specified model
     * @var string
     */
    public $relation = '';

    /**
     * relation attribute for specified relation
     * @var string
     */
    public $relationAttribute = '';

    public $uniqueId = null;

    public $prefix = '';

    public function run() {
        $this->uniqueId = Tools::uniqueId();

        $data = array(
            'form'=>$this->form,
            'model'=>$this->model,
            'attribute'=>$this->attribute,
        );
        $this->render('form/uploader', $data);
    }

}