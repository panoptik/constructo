<?php
/**
 * Date: 27.03.14
 * Time: 12:51
  */

class WLangField extends WFormFields {

    public function run() {
        /** @var CWidgetFactory $widgetFactory */
        $widgetFactory = Yii::app()->widgetFactory;


        $data = $this->data;
        if(!isset($data['type']))
            throw new CException('You must specified type for language fields');
        $type = $data['type'];
        unset($data['type']);

        $languages = Yii::app()->params['languages'];

        foreach($languages as $code => $name) {

            // merge container options
            $containerOptions = array(
                'class' => '',
            );
            if(isset($widgetFactory->widgets[$type])) {
                $w = $widgetFactory->widgets[$type];
                $containerOptions = isset($w['containerOptions']) ? $w['containerOptions'] : array();
            }
            $containerOptions = CMap::mergeArray($containerOptions, $this->containerOptions);
            $containerOptions['class'] .= ' language-field-container ' . $code;

            $htmlOptions = $this->htmlOptions;
            $htmlOptions['value'] = $this->model->getLangAttribute($this->attribute, $code);

            $this->widget($type, array(
                'form'=>$this->form,
                'model'=>$this->model,
                'attribute'=>'[translations][' . $code . ']'.$this->attribute,
                'data'=>$data,
                'htmlOptions' => $htmlOptions,
                'containerOptions' => $containerOptions,
            ));
        }
    }

} 