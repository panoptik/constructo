<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 20.06.13 Time: 5:26
 */

class WMultipleFields extends CWidget {

    private static $_initScripts = false;

    /**
     * cached view paths for non theme files
     * @var array
     */
    private static $_viewPaths = array();

    /**
     * cached view paths for theme files
     * @var array
     */
    private static $_themeViewPaths = array();

    public $view = 'widgets/form/multipleFields';

    public $model;

    public $relationAttribute;

    public $fieldAttribute;

    public $attribute;
    public $attributeLabel;

    public $parentIndex = '';
    public $parentPrefix = null;

    public $startIndex = 0;

    public $uniqueId = null;

    /**
     * run widget method
     */
    public function run() {

        $this->uniqueId = $this->generateUniqueId();

        if(!self::$_initScripts) {
            self::$_initScripts = true;
            $js = "
            $('body')
                .off('click', '.multipleFields .roundBtn')
                .on('click', '.multipleFields .roundBtn', function(){
                var btn = $(this),
                    container = btn.closest('.multipleFields'),
                    cId = container.attr('id');
                // add button
                if(btn.hasClass('add')) {
                    var maxIndex = 0,
                        // children items
                        ch = container.find('.fieldItem.'+cId);
                    ch.each(function(){
                        if($(this).data('index') > maxIndex) {
                            maxIndex = $(this).data('index');
                        }
                    });
                    $.post('".$this->controller->createUrl('addRelationField')."', {
                        modelname: container.data('modelname'),
                        relation: container.data('relation'),
                        label: container.data('label'),
                        attribute: container.data('attribute'),
                        field: container.data('field'),
                        parentIndex: container.data('parent'),
                        parentPrefix: container.data('prefix'),
                        startIndex: maxIndex + 1
                    }, function(data) {
                        var curContainer = $(data),
                            curId = curContainer.attr('id'),
                            fieldItem = curContainer.find('.fieldItem.'+curId);
                            fieldItem.removeClass(curId);

                        fieldItem.addClass(cId);
                        container.find('.fieldItem.'+cId+':last').after(fieldItem);
                        $('.fieldItem.remove').remove();
                    });
                }
                if(btn.hasClass('remove')) {
                    $.post('".$this->controller->createUrl('removeRelationField')."', {
                        modelname: container.data('modelname'),
                        relation: container.data('relation'),
                        id: btn.closest('.fieldItem').data('id')
                    }, function() {
                        if(container.find('.fieldItem.'+cId).length > 1) {
                            btn.closest('.fieldItem').fadeOut(200, function(){
                                $(this).remove();
                            });
                        } else {
                            btn.closest('.fieldItem').fadeOut(200, function(){
                                $(this).addClass('remove');
                                container.find('.add.'+cId).click();
                            });
                        }
                    });
                }
            });
            ";
            /* @var CClientScript $cs */
            $cs = Yii::app()->clientScript;
            $cs->registerScript('wmultiplefields', $js, CClientScript::POS_READY);
        }

        $models = $this->model->{$this->relationAttribute};
        if(!count($models)) {
            $activeRelation = $this->model->getActiveRelation($this->relationAttribute);
            $className = $activeRelation->className;
            $models[$this->startIndex] = new $className();
        }

        $modelname = get_class($this->model);

        $parentPrefix = '';
        $parentIndex = $this->parentIndex;

        if($this->parentPrefix !== null) {
            $parentPrefix = $this->parentPrefix;
        } elseif($this->parentIndex) {
            $parentPrefix = $modelname.'['.$this->parentIndex.']';
        } else {
            $parentPrefix = $modelname;
        }

        $data = array(
            'parentIndex' => $parentIndex,
            'modelname' => $modelname,
            'models' => $models,
            'parentPrefix' => $parentPrefix,
        );
        $this->render($this->view, $data);
    }

    /**
     * generate random unique id
     * @return string
     */
    private function generateUniqueId() {
        $mt = microtime(true);
        $number = (string)round($mt, 5);
        $number = substr(str_replace('.','_',$number),6);
        $randStr = substr(md5(rand(0, time())),0,6);
        $id = 'multiple_field_'.$number.'_'.$randStr;
        return $id;
    }

    /**
     * override method to set default view path
     * @param bool $checkTheme
     * @return string
     */
    public function getViewPath($checkTheme=false)
    {
        $className=get_class($this);
        if($checkTheme && isset(self::$_themeViewPaths[$className])) {
            return self::$_themeViewPaths[$className];
        } elseif(isset(self::$_viewPaths[$className])) {
            return self::$_viewPaths[$className];
        } else
        {
            if($checkTheme && ($theme=Yii::app()->getTheme())!==null)
            {
                $path=$theme->getViewPath();
                if(($module=$this->controller->getModule())!==null)
                    if(is_dir($path.DIRECTORY_SEPARATOR.$module->getId()))
                        return self::$_themeViewPaths[$className]=$path.DIRECTORY_SEPARATOR.$module->getId();
                if(is_dir($path))
                    return self::$_themeViewPaths[$className]=$path;
            }
            $path=$basePath=Yii::app()->getViewPath();
            if(($module=$this->controller->getModule())!==null)
                $path=$module->getViewPath();
            $path .= DIRECTORY_SEPARATOR;
            if(is_dir($path)) {
                return self::$_viewPaths[$className]=$path;
            }

            $class=new ReflectionClass($className);
            return self::$_viewPaths[$className]=dirname($class->getFileName()).DIRECTORY_SEPARATOR.'views';
        }
    }

} /*?>
<div class="formRow multipleFields" data-modelname="Contractor" data-relation="contactNames" data-label="Contact Name" data-attribute="name" data-field="contacts" data-parent="" data-prefix="Contractor[]" id="multiple_field_3564_4379_3b0766">
    <div class="grid3 body">
        <label style="margin-right: 10px;" for="">Contact Name</label>        <a href="javascript:{}" class="add roundBtn multiple_field_3564_4379_3b0766"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
    </div>
    <div class="grid9">
        <div class="fieldItem multiple_field_3564_4379_3b0766" data-index="0" data-id="">
            <input type="text" name="Contractor[contacts][0][name]">            <input type="hidden" name="Contractor[contacts][0][id]" id="Contractor_contacts_0_id">            <a href="javascript:{}" class="remove roundBtn"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
            <div>
                <div class="formRow multipleFields" data-modelname="ContactName" data-relation="emails" data-label="Contact Email" data-attribute="data" data-field="contactEmails" data-parent="0" data-prefix="Contractor[contacts][0]" id="multiple_field_3564_442_03cc00">
                    <div class="grid3 body">
                        <label for="">Contact Email</label>        <a href="javascript:{}" class="add roundBtn multiple_field_3564_442_03cc00"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                    </div>
                    <div class="grid9">
                        <div class="fieldItem multiple_field_3623_4242_dd7b58 multiple_field_3564_442_03cc00" data-index="12" data-id="">
                            <input type="text" name="Contractor[contacts][0][contactEmails][12][data]">        <input type="hidden" name="Contractor[contacts][0][contactEmails][12][id]" id="Contractor_contacts_0_contactEmails_12_id">        <a href="javascript:{}" class="remove roundBtn"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>
            <div>
                <div class="formRow multipleFields" data-modelname="ContactName" data-relation="phones" data-label="Contact Phone" data-attribute="data" data-field="contactPhones" data-parent="0" data-prefix="Contractor[contacts][0]" id="multiple_field_3564_4443_166a14">
                    <div class="grid3 body">
                        <label for="">Contact Phone</label>        <a href="javascript:{}" class="add roundBtn multiple_field_3564_4443_166a14"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                    </div>
                    <div class="grid9">
                        <div class="fieldItem multiple_field_3618_0453_4bef7d multiple_field_3564_4443_166a14" data-index="6" data-id="">
                            <input type="text" name="Contractor[contacts][0][contactPhones][6][data]">        <input type="hidden" name="Contractor[contacts][0][contactPhones][6][id]" id="Contractor_contacts_0_contactPhones_6_id">        <a href="javascript:{}" class="remove roundBtn"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>
        </div>
        <div class="fieldItem multiple_field_3628_0893_6f797f multiple_field_3564_4379_3b0766" data-index="1" data-id="">
            <input type="text" name="Contractor[contacts][1][name]">            <input type="hidden" name="Contractor[contacts][1][id]" id="Contractor_contacts_1_id">            <a href="javascript:{}" class="remove roundBtn"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
            <div>
                <div class="formRow multipleFields" data-modelname="ContactName" data-relation="emails" data-label="Contact Email" data-attribute="data" data-field="contactEmails" data-parent="1" data-prefix="Contractor[contacts][1]" id="multiple_field_3628_0919_a8d77e">
                    <div class="grid3 body">
                        <label for="">Contact Email</label>        <a href="javascript:{}" class="add roundBtn multiple_field_3628_0919_a8d77e"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                    </div>
                    <div class="grid9">

                    </div>
                    <div class="clear"></div>
                </div>

            </div>
            <div>
                <div class="formRow multipleFields" data-modelname="ContactName" data-relation="phones" data-label="Contact Phone" data-attribute="data" data-field="contactPhones" data-parent="1" data-prefix="Contractor[contacts][1]" id="multiple_field_3628_0939_166e01">
                    <div class="grid3 body">
                        <label for="">Contact Phone</label>        <a href="javascript:{}" class="add roundBtn multiple_field_3628_0939_166e01"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
                    </div>
                    <div class="grid9">

                    </div>
                    <div class="clear"></div>
                </div>

            </div>
        </div>
        <div class="fieldItem multiple_field_3628_0919_a8d77e multiple_field_3564_4379_3b0766" data-index="0" data-id="">
            <input type="text" name="Contractor[contacts][1][contactEmails][0][data]">
            <input type="hidden" name="Contractor[contacts][1][contactEmails][0][id]" id="Contractor_contacts_1_contactEmails_0_id">                 <a href="javascript:{}" class="remove roundBtn">
                <div class="glyph"><div class="fs1 iconb" data-icon=""></div></div>
            </a>
        </div>
        <div class="fieldItem multiple_field_3628_0939_166e01 multiple_field_3564_4379_3b0766" data-index="0" data-id="">
            <input type="text" name="Contractor[contacts][1][contactPhones][0][data]">
            <input type="hidden" name="Contractor[contacts][1][contactPhones][0][id]" id="Contractor_contacts_1_contactPhones_0_id">
            <a href="javascript:{}" class="remove roundBtn">
                <div class="glyph"><div class="fs1 iconb" data-icon=""></div></div>
            </a>
        </div>
    </div>
    <div class="clear"></div>
</div>*/