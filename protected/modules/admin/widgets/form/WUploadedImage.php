<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 15.06.13 Time: 19:03
 */

class WUploadedImage extends WFormFields {


    public $imgUrl = null;

    /**
     * rendered view
     * @var string
     */
    public $view = '/form/uploadedImage';

    /**
     * flag if is multiple fields
     * @var bool
     */
    public $multiple = false;

    /**
     * name of model relation
     * @var string
     */
    public $relation = null;

    /**
     * attribute for related model
     * @var string
     */
    public $relationAttribute = 'image';

    /**
     * custom image url
     * @var string
     */
    public $image = null;

    /**
     * contain multiple mode index of fields
     * @var string
     */
    private $_multipleIndex = '';

    /**
     * fields name prefix
     * @var string
     */
    public $prefix = '';

    public function run() {
        if(!$this->imgUrl)
            $this->imgUrl = $this->model->getImage($this->attribute, 'x150');

        if($this->image)
            $value = $this->image;
        else
            $value = $this->model->{$this->attribute};

        /* @var CActiveRecord $model */
        $model = $this->model;
        if($this->relation) {
            /* @var CActiveRelation $relation */
            $relation = $model->getActiveRelation($this->relation);

            $model = CActiveRecord::model($relation->className)->findByAttributes(
                array(
                    $this->relationAttribute => $value,
                )
            );

            if(!$model) {
                $model = new $relation->className;
            }
        }

        if(!$this->prefix)
            $this->prefix = get_class($this->model);

        $data = array(
            'imgUrl' => $this->imgUrl,
            'id' => $model->id,
            'currentModelClass' => get_class($model),
            'model' => $this->model,
            'attribute' => $this->attribute,
            'value' => $value,
        );
        $this->render($this->view, $data);
    }

    protected function getMultipleIndex($attribute = null) {
        if(!$attribute)
            $attribute = $this->relationAttribute;
        if($this->multiple) {
            if(!$this->_multipleIndex) {
                $uniqueId = Tools::uniqueId();
                $this->_multipleIndex = "[{$uniqueId}]";
            }
            return $this->_multipleIndex."[$attribute]";
        }
        return '';
    }

}