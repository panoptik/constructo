<?php
/**
 * Created by PhpStorm.
 * User: senseless-long-name
 * Date: 3/28/14
 * Time: 9:44 AM
 */


class WFlashMessages extends Widget {

    public function run(){
        $flashes = Yii::app()->user->getFlashes();
        $class = array(
            'error' => 'alert-danger',
            'success' => 'alert-success',
            'info' => 'alert-info',
            'warning' => 'alert-warning'
        );
        $keys = array(
            'error' => Yii::t('t','Oops!'),
            'success' => Yii::t('t','Success!'),
            'info' => Yii::t('t','Useful Info!'),
            'warning' => Yii::t('t','Be Careful!'),
        );
        $rf = array();
        foreach ($flashes as $key =>$message){
            if (isset($class[$key]))
                $m['class'] = $class[$key];
            else
                $m['class'] = 'alert-'.$key;
            if (isset($keys[$key]))
                $m['key'] = $keys[$key];
            else
                $m['key'] = $key;
            $m['message'] = $message;
            $rf[] = $m;
        }
        $data = array(
            'flashes'=>$rf
        );
        $this->render('flashMessages', $data);
    }
} 