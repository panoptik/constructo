<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 22.05.13 Time: 1:24
 */
Yii::import('admin.extensions.uploader.EAjaxUpload');
class AjaxUpload extends EAjaxUpload {

    /**
     * @var CActiveRecord
     */
    public $model;

    /**
     * @var string
     */
    public $attribute;

    /**
     * content type
     * @var string
     */
    public $type = 'image';

    /**
     * unique identifier for container tag
     * @var string
     */
    public $uniqueId = null;

    /**
     * model relation
     * @var string
     */
    public $relation = null;

    /**
     * model relation attribute
     * @var string
     */
    public $relationAttribute = null;

    /**
     * allow upload multiple file at the same time
     * @var bool
     */
    public $multiple = false;

    /**
     * fields name prefix
     * @var string
     */
    public $prefix = '';

    public function init() {
        if($this->model && $this->attribute) {
            $this->id = get_class($this->model).'_'.$this->attribute;
        }
        if($this->uniqueId) {
            $this->id .= '_'.$this->uniqueId;
        }
        if(isset($this->config['multiple'])) {
            $this->multiple = $this->config['multiple'];
        } elseif($this->multiple) {
            $this->config['multiple'] = true;
        }
    }

    public function run() {
        $model = $this->model;
        $attribute = $this->attribute;

        $this->config['sizeLimit'] = Yii::app()->params->uploadSizeLimit;

        switch($this->type) {
            case 'image':
                $this->config['action'] = $this->controller->createUrl('upload',
                    array(
                        'attribute'=>$attribute,
                        'multiple'=>$this->multiple,
                        'relation'=>$this->relation,
                        'relationAttribute'=>$this->relationAttribute,
                        'model'=>get_class($this->model),
                        'prefix'=>$this->prefix,
                    ));
                $this->config['allowedExtensions'] = array('jpg', 'jpeg', 'png', 'gif');
                $this->config['onComplete'] = $this->getCompleteCallbackJS();
                break;
            default:
                throw new CException('Unsupported type "'.$this->type.'"');
        }

        $deleteUrl = $this->controller->createUrl('deleteFile');
        /* @var CClientScript $cs */
        if(empty($this->config['deleteCallback'])) {
            $deleteCallback = "
            function() {
                if(confirm('Удалить?')) {
                    var elem = $(this).closest('.uploadedItem'),
                        fileName = elem.find('.fileName').val(),
                        modelName = elem.data('modelname');
                    $.post('{$deleteUrl}',{
                        fileName: fileName,
                        id: elem.data('id'),
                        modelName: modelName,
                        attribute: elem.data('attribute')
                    });
                    elem.fadeOut(100, function() {
                        $(this).remove();
                        $('#{$this->id}').find('.qq-upload-success').remove();
                    });
                    $('#{$this->id}').fadeIn();
                }
            }";
        } else {
            $deleteCallback = $this->config['deleteCallback'];
        }


        $cs = Yii::app()->clientScript;
        $js = "
        $('body').on('click', '#{$this->id}_files .close', {$deleteCallback});";
        if($model->$attribute && !$this->multiple) {
            $js .= "$('#{$this->id}').hide();";
        }
        $cs->registerScript($this->id, $js, CClientScript::POS_READY);
        $css = <<<CSS
            .qq-upload-list { display: none;}
            .uploadedItem { display: inline-block; width: 200px; height: 150px; margin: 5px; vertical-align: top; }
            .uploadedItem img { max-width: 200px; max-height: 150px; display: block; }

CSS;
        $cs->registerCss('#qq_upload', $css);
        parent::run();

    }

    protected function getCompleteCallbackJS() {
        $js  = "js:function(id, fileName, responseJSON){"."\n";
        $js .= "    if(responseJSON.success) {"."\n";

        if(!$this->multiple) {
            $js .= "        $('#{$this->id}').fadeOut();"."\n";
        }

        $js .= "        var container = $('#{$this->id}_files');"."\n";
        $js .= "        container.append(responseJSON.html);"."\n";
        $js .= "        $('#{$this->id}_files').find('.qq-upload-success').remove();\n";
        $js .= "    } else {"."\n";
        $js .= "        alert('An error occurred while upload');"."\n";
        $js .= "    }"."\n";
        $js .= "}"."\n";
        return $js;
    }

}