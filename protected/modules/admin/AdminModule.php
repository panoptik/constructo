<?php

class AdminModule extends CWebModule
{
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.components.*',
            'zii.widgets.grid.CGridView',
            'admin.extensions.wysiwyg.ImperaviRedactorWidget',
            'admin.widgets.*',
            'admin.widgets.form.*'
		));

        $this->_configureApplication();

	}

	public function beforeControllerAction($controller, $action) {
		if(parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
        }
	}

    private function _configureApplication() {
        // get module config file
        $configDir = Yii::getPathOfAlias('admin.config');
        $configFile = $configDir.'/main.php';
        if(file_exists($configFile)) {
            $config = require $configFile;
            Yii::app()->configure($config);
        }

        // check if exists theme config file and configure main application config
        if(Yii::app()->theme !== null) {
            $themeConfig = $configDir.'/'.Yii::app()->theme->name.'.php';
            if(file_exists($themeConfig)) {
                $config = require $themeConfig;
                Yii::app()->configure($config);
            }
        }
    }
}
