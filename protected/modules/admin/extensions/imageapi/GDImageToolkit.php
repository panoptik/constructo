<?php
/**
 *
 */
class GDImageToolkit extends ImageToolkit {

    /**
     * @var int
     */
    public static $qualityJPEG = 75;

    /**
     * Retrieve information about the toolkit.
     * @return array
     */
    public function toolkit_info() {
        return array('name' => 'gd', 'title' => 'GD2 image manipulation toolkit');
    }

    /**
     * Verify GD2 settings (that the right version is actually installed).
     * @return boolean A boolean indicating if the GD toolkit is avaiable on this machine.
     */
    public function toolkit_check() {
        if ($check = get_extension_funcs('gd')) {
            if (in_array('imagegd2', $check)) {
                // GD2 support is available.
                return true;
            }
        }
        return false;
    }

    /**
     * GD helper function to create an image resource from a file.
     * @param string $file A string file path where the iamge should be saved.
     * @param string $extension A string containing one of the following extensions: gif, jpg, jpeg, png.
     * @return resource|bool An image resource, or FALSE on error.
     */
    public function open($file, $extension) {
        $extension = str_replace('jpg', 'jpeg', $extension);
        $open_func = 'imageCreateFrom' . $extension;
        if (!function_exists($open_func)) {
            return false;
        }
        if (@$open_func($file))
            return $open_func($file);
        else
            return false;
    }

    /**
     * GD helper to write an image resource to a destination file.
     * @param resource $res An image resource created with open().
     * @param string $destination A string file path where the iamge should be saved.
     * @param string $extension A string containing one of the following extensions: gif, jpg, jpeg, png.
     * @return boolean Boolean indicating success.
     */
    public function close($res, $destination, $extension) {
        $extension = str_replace('jpg', 'jpeg', $extension);
        $close_func = 'image' . $extension;
        if (!function_exists($close_func)) {
            return false;
        }
        if ($extension == 'jpeg') {
            $close_func($res, $destination, self::$qualityJPEG);
        } else {
            $close_func($res, $destination);
        }
        umask(0);
        chmod($destination, 0777);
        return true;
    }

    /**
     * Scale an image to the specified size using GD.
     * @param string $source
     * @param string $destination
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function resize($source, $destination, $width, $height) {
        if (!file_exists($source)) {
            return false;
        }
        $info = $this->getInfo($source);
        if (!$info) {
            return false;
        }
        $im = $this->open($source, $info['extension']);
        if (!$im) {
            return false;
        }
        if ($info['width'] <= $width and $info['height'] <= $height) {
            $result = $this->close($im, $destination, $info['extension']);
            imagedestroy($im);
            return $result;
        }
        $res = imagecreatetruecolor($width, $height);
        if ($info['extension'] == 'png') {
            $transparency = imagecolorallocatealpha($res, 0, 0, 0, 127);
            imagealphablending($res, false);
            imagefilledrectangle($res, 0, 0, $width, $height, $transparency);
            imagealphablending($res, true);
            imagesavealpha($res, true);
        } elseif ($info['extension'] == 'gif') {
            // If we have a specific transparent color.
            $transparency_index = imagecolortransparent($im);
            if ($transparency_index >= 0 and $transparency_index < imagecolorstotal($im)) {
                // Get the original image's transparent color's RGB values.
                $transparent_color = imagecolorsforindex($im, $transparency_index);
                // Allocate the same color in the new image resource.
                $transparency_index = imagecolorallocate($res, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                // Completely fill the background of the new image with allocated color.
                imagefill($res, 0, 0, $transparency_index);
                // Set the background color for new image to transparent.
                imagecolortransparent($res, $transparency_index);
                // Find number of colors in the images palette.
                $number_colors = imagecolorstotal($im);
                // Convert from true color to palette to fix transparency issues.
                imagetruecolortopalette($res, true, $number_colors);
            }
        }
        imagecopyresampled($res, $im, 0, 0, 0, 0, $width, $height, $info['width'], $info['height']);
        $result = $this->close($res, $destination, $info['extension']);
        imagedestroy($res);
        imagedestroy($im);
        return $result;
    }

    /**
     * Rotate an image the given number of degrees.
     * @param string $source
     * @param string $destination
     * @param int $degrees
     * @param int $background
     * @return bool
     */
    public function rotate($source, $destination, $degrees, $background = 0x000000) {
        if (!function_exists('imagerotate')) {
            return false;
        }
        $info = $this->getInfo($source);
        if (!$info) {
            return false;
        }
        $im = $this->open($source, $info['extension']);
        if (!$im) {
            return false;
        }
        $res = imagerotate($im, $degrees, $background);
        $result = $this->close($res, $destination, $info['extension']);
        return $result;
    }

    /**
     * Crop an image using the GD toolkit.
     * @param string $source
     * @param string $destination
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function crop($source, $destination, $x, $y, $width, $height) {
        $info = $this->getInfo($source);
        if (!$info) {
            return false;
        }
        $im = $this->open($source, $info['extension']);
        if ($info['width'] <= $width and $info['height'] <= $height) {
            $result = $this->close($im, $destination, $info['extension']);
            imagedestroy($im);
            return $result;
        } elseif($info['width'] <= $width) {
            $new_width = $info['width'];
            $new_height = $height;
        } elseif($info['height'] <= $height) {
            $new_width = $width;
            $new_height = $info['height'];
        } else {
            $new_width = $width;
            $new_height = $height;
        }
        $res = imagecreatetruecolor($new_width, $new_height);
        imagecopy($res, $im, 0, 0, $x, $y, $new_width, $new_height);
        $result = $this->close($res, $destination, $info['extension']);
        imagedestroy($res);
        imagedestroy($im);
        return $result;
    }

    /**
     * @param string $source
     * @param string $watermarkPath
     * @return bool
     */
    public function watermark($source, $watermarkPath) {
        $info = $this->getInfo($source);
        $watermarkInfo = $this->getInfo($watermarkPath);
        switch ($info['extension']) {
            case 'gif':
                if (@imagecreatefromgif($source))
                    $image = imagecreatefromgif($source);
                else
                    return false;
                break;
            case 'jpg':
                if (@imagecreatefromjpeg($source))
                    $image = imagecreatefromjpeg($source);
                else
                    return false;
                break;
            case 'png':
                if (@imagecreatefrompng($source))
                    $image = imagecreatefrompng($source);
                else
                    return false;
                break;
            default:
                return false;
        }
        $watermark = imagecreatefrompng($watermarkPath);
        $startwidth = (($info['width'] - $watermarkInfo['width']) / 2);
        $startheight = (($info['height'] - $watermarkInfo['height']) / 2);
        imagecopy($image, $watermark, $startwidth, $startheight, 0, 0, $watermarkInfo['width'], $watermarkInfo['height']);
        $result = $this->close($image, $source, $info['extension']);
        imagedestroy($image);
        imagedestroy($watermark);
        return $result;
    }
}