<?php
/**
 *
 */
abstract class ImageToolkit extends CComponent {

    public function getInfo($file) {
        if (!is_file($file)) {
            return false;
        }
        $details = null;
        $data = @getimagesize($file);
        $file_size = @filesize($file);
        if (isset($data) and is_array($data)) {
            $extensions = array('1' => 'gif', '2' => 'jpg', '3' => 'png');
            $extension = array_key_exists($data[2], $extensions) ? $extensions[$data[2]] : '';
            $details = array(
                'width' => $data[0],
                'height' => $data[1],
                'extension' => $extension,
                'file_size' => $file_size,
                'mime_type' => $data['mime'],
            );
        }
        return $details;
    }

    /**
     * Metadata of the toolkit
     */
    abstract public function toolkit_info();

    /**
     * Checks if the toolkit is operative with the actual configuration
     * @return boolean whether the toolkit is operative
     */
    abstract public function toolkit_check();

    /**
     * Create an image resource from a file.
     * @param string $file A string file path where the image should be saved.
     * @param string $extension A string containing one of the following extensions: gif, jpg, jpeg, png.
     * @return resource An image resource, or FALSE on error.
     */
    abstract public function open($file, $extension);

    /**
     * Write an image resource to a destination file.
     * @param resource $res An image resource created with image_gd_open().
     * @param string $destination A string file path where the iamge should be saved.
     * @param string $extension A string containing one of the following extensions: gif, jpg, jpeg, png.
     * @return bool Boolean indicating success.
     */
    abstract public function close($res, $destination, $extension);

    /**
     * Scales an image to the given width and height while maintaining aspect ratio.
     * The resulting image can be smaller for one or both target dimensions.
     * @param string $source The file path of the source image.
     * @param string $destination The file path of the destination image.
     * @param int $width The target width, in pixels.
     * @param int $height The target height, in pixels.
     * @return bool TRUE or FALSE, based on success.
     */
    abstract public function resize($source, $destination, $width, $height);

    /**
     * Rotate an image the given number of degrees.
     * @param string $source
     * @param string $destination
     * @param int $degrees
     * @param int $background
     * @return bool
     */
    abstract public function rotate($source, $destination, $degrees, $background = 0x000000);

    /**
     * Crop an image using the GD toolkit.
     * @param string $source
     * @param string $destination
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return bool
     */
    abstract public function crop($source, $destination, $x, $y, $width, $height);
}