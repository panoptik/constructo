<?php
/**
 *
 */
class CImage extends CApplicationComponent {

    /**
     * @var ImageToolkit
     */
    public $toolkit;

    /**
     * @return void
     */
    public function init() {
        parent::init();
        //Yii::import('application.extensions.imageapi.ImageToolkit');
        //Yii::import('application.extensions.imageapi.GDImageToolkit');
        require_once('ImageToolkit.php');
        require_once('GDImageToolkit.php');
        $this->toolkit = new GDImageToolkit();
    }

    /**
     * @param $file
     * @return array|bool
     */
    public function getInfo($file) {
        return $this->toolkit->getInfo($file);
    }

    /**
     * @param string $file
     * @param string|array $params
     * @param string $action
     * @param bool $watermark
     * @return bool|string
     */
    public function createPath($file, $params, $action = 'scaleAndCrop', $watermark = false) {
        if (!file_exists($file)) {
            return false;
        }
        if (!is_array($params)) {
            $directory = $params;
            $params = explode('x', $params);
            $params['width'] = $params[0];
            $params['height'] = $params[1];
        } else {
            if (isset($params['degress'])) {
                $directory = 'rotate_' . $params['degrees'];
            } elseif (isset($params['x'])) {
                $directory = 'crop_' . $params['width'] . 'x' . $params['height'];
            } else {
                $directory = $params['width'] . 'x' . $params['height'];
            }
        }
        $basename = basename($file);
        $targetPath = Yii::getPathOfAlias(Yii::app()->params['uploadFolder']) . '/cache/' . $directory;
        $targetFile = $targetPath . '/' . $basename;
        umask(0);
        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }
        if (file_exists($targetFile)) {
            return $targetFile;
        } else {
            copy($file, $targetFile);
            switch ($action) {
                case 'scaleAndCrop':
                    $this->scaleAndCrop($targetFile, $targetFile, $params['width'], $params['height']);
                    break;
                case 'scale':
                    $this->scale($targetFile, $targetFile, $params['width'], $params['height']);
                    break;
                case 'resize':
                    $this->resize($targetFile, $targetFile, $params['width'], $params['height']);
                    break;
                case 'rotate':
                    $this->rotate($targetFile, $targetFile, $params['degrees'], $params['background']);
                    break;
                case 'crop':
                    $this->crop($targetFile, $targetFile, $params['x'], $params['y'], $params['width'], $params['height']);
                    break;
            }
            if ($watermark) {
                $this->watermark($targetFile);
            }
            return $targetFile;
        }
    }

    /**
     * @param string $file
     * @param string|array $params
     * @param string $action
     * @return bool|string
     */
    public function createUrl($file = null, $params = null, $action = 'scaleAndCrop', $watermark = false) {
        if (!$file) {
            $file = Yii::getPathOfAlias(Yii::app()->params['uploadFolder']) . '/noimage.png';
        }
        if (!file_exists($file)) {
            return false;
        }
        if ($params) {
            $generatedPath = $this->createPath($file, $params, $action, $watermark);
        } else {
            $generatedPath = $file;
        }
        $generatedUrl = $this->convertPathToUrl($generatedPath);
        return $generatedUrl;
    }

    /**
     * @param string $source
     * @param string $destination
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function scaleAndCrop($source, $destination, $width, $height) {
        $info = self::getInfo($source);
        if (!$info['width'] or !$info['height']) {
            $source = Yii::getPathOfAlias(Yii::app()->params['uploadFolder']) . '/noimage.png';
            $info = self::getInfo($source);
        }
        $scale = max($width / $info['width'], $height / $info['height']);
        $x = round(($info['width'] * $scale - $width) / 2);
        $y = round(($info['height'] * $scale - $height) / 2);
        if ($this->toolkit->resize($source, $destination, $info['width'] * $scale, $info['height'] * $scale)) {
            if (!$height) {
                return $this->toolkit->crop($destination, $destination, $x, $y, $width, round(($info['height']/$info['width'])*$width));
            } elseif(!$width) {
                return $this->toolkit->crop($destination, $destination, $x, $y, round(($info['width']/$info['height'])*$height), $height);
            } else {
                return $this->toolkit->crop($destination, $destination, $x, $y, $width, $height);
            }
        }
        return false;
    }

    /**
     * @param string $source
     * @param string $destination
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function scale($source, $destination, $width, $height) {
        $info = self::getInfo($source);
        if ($width >= $info['width'] and $height >= $info['height']) {
            return false;
        }
        $aspect = $info['height'] / $info['width'];
        if ($aspect < $height / $width) {
            $width = (int)min($width, $info['width']);
            $height = (int)round($width * $aspect);
        } else {
            $height = (int)min($height, $info['height']);
            $width = (int)round($height / $aspect);
        }
        return $this->toolkit->resize($source, $destination, $width, $height);
    }

    /**
     * @param string $source
     * @param string $destination
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function resize($source, $destination, $width, $height) {
        return $this->toolkit->resize($source, $destination, $width, $height);
    }

    /**
     * @param string $source
     * @param string $destination
     * @param int $degrees
     * @param int $background
     * @return bool
     */
    public function rotate($source, $destination, $degrees, $background = 0x000000) {
        return $this->toolkit->rotate($source, $destination, $degrees, $background);
    }

    /**
     * @param string $source
     * @param string $destination
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function crop($source, $destination, $x, $y, $width, $height) {
        return $this->toolkit->crop($source, $destination, $x, $y, $width, $height);
    }

    /**
     * @param string $path
     * @return string
     */
    public function convertPathToUrl($path, $absolute = false) {
        $webrootPath = Yii::getPathOfAlias('webroot');
        if (strpos($path, $webrootPath) !== false) {
            $path = substr($path, strlen($webrootPath));
        }
        return str_replace('\\', '/', Yii::app()->getBaseUrl($absolute) . $path);
    }

    public function watermark($source) {
        $watermarkPath = Yii::getPathOfAlias('webroot.static.uploads') . '/watermark.png';
        $info = self::getInfo($source);
        $watermarkInfo = self::getInfo($watermarkPath);
        if ($info['width'] < $watermarkInfo['width'] or $info['height'] < $watermarkInfo['height']) {
            return false;
        }
        $this->toolkit->watermark($source, $watermarkPath);

    }
}