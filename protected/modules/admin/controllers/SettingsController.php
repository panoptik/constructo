<?php
/**
 * Created by PhpStorm.
 * User: senseless-long-name
 * Date: 3/27/14
 * Time: 3:24 PM
 */

class SettingsController extends AdminCrudController {

    public function init() {
        parent::init();
        $this->modelName = 'Settings';
        $this->title = Yii::t('t', 'Settings');
        $this->useDefaultIndexView = true;
        $this->useDefaultFormView = true;
//        'id' => 'ID',
//        'key' => 'Key',
//        'value' => 'Value',
//        'description' => 'Description',
//        'group' => 'Group',
//        'type' => 'Type',
//        'sort' => 'Sort',
        $this->formFields = array(
            'key' => array(
                'widgetClass' => 'WTextField',
            ),
            'value' => array(
                'widgetClass' => 'WTextField',
            ),
            'description' => array(
                'widgetClass' => 'WTextField',
            ),
            'group' => array(
                'widgetClass' => 'WTextField',
            ),
            'type' => array(
                'widgetClass' => 'WTextField',
            ),
            'sort' => array(
                'widgetClass' => 'WTextField',
            ),
        );

        $this->gridData = array(
            'columns' => array(
                'id' => array(
                    'name' => 'id',
                ),
                'key' => array(
                    'name' => 'key',
                ),
                'value' => array(
                    'name' => 'value',
                ),
                'description' => array(
                    'name' => 'description',
                ),
                'group' => array(
                    'name' => 'group',
                ),
                'type' => array(
                    'name' => 'type',
                ),
                'sort' => array(
                    'name' => 'sort',
                ),
            ),
        );
    }
} 