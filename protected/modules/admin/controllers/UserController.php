<?php
/**
 * Date: 25.03.14
 * Time: 13:38
  */

class UserController extends AdminCrudController {

    public function init() {
        parent::init();
        $this->modelName = 'User';
        $this->title = Yii::t('t', 'Users');
        $this->useDefaultIndexView = true;
        $this->useDefaultFormView = true;

        $this->formFields = array(
            'email' => array(
                'widgetClass' => 'WTextField',
            ),
            'password' => array(
                'widgetClass' => 'WPasswordField',
                'htmlOptions' => array(
                    'value' => ''
                )
            ),
            'passwordConfirm' => array(
                'widgetClass' => 'WPasswordField',
                'htmlOptions' => array(
                    'value' => ''
                )
            ),
            'firstName' => array(
                'widgetClass' => 'WTextField',
            ),
            'lastName' => array(
                'widgetClass' => 'WTextField',
            ),
            'facebookId' => array(
                'widgetClass' => 'WTextField',
            ),
            'image' => array(
                'widgetClass' => 'WUploader',
            ),
            'status' => array(
                'widgetClass' => 'WDropDownList',
                'data' => User::$statuses
            ),
            'role' => array(
                'widgetClass' => 'WDropDownList',
                'data' => User::$roles
            )
        );

        $this->gridData = array(
            'columns' => array(
                'id' => array(
                    'name' => 'id',
                    'htmlOptions'=>array(
                        'style'=>'width: 60px;'
                    ),
                ),
                'email' => array(
                    'name' => 'email',
                    'type' => 'raw',
                    'value' => 'CHtml::link($data->email, $this->grid->owner->createUrl("update",array("id"=>$data->id)),array("class"=>"update"))',
                ),
                'Full Name' => array(
                    'name' => 'fullName'
                ),
                'facebookId' => array(
                    'name' => 'facebookId',
                ),
                'status' => array(
                    'name' => 'status',
                    'value' => '$data->statusTitle'
                ),
                'role' => array(
                    'name' => 'role',
                ),
            ),
        );
    }

} 