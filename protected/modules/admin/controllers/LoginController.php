<?php
/**
 * Date: 25.03.14
 * Time: 11:57
  */

class LoginController extends AdminController {

    public $layout = '/layouts/layout';

    /**
     * access control settings
     * used RBAC
     * @return array
     */
    public function accessRules() {
        return array(
            array(
                'deny',
                'roles' => array('admin'),
                'deniedCallback' => function(){
                    Yii::app()->request->redirect('/admin');
                },
            ),
            array(
                'allow',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {
        $model = new LoginForm();

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()){
                $this->redirect($this->redirect('/admin'));
            }
        }

        $this->render('index', array('model'=>$model));
    }

} 