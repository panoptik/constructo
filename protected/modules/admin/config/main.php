<?php
return array(
    'theme'=>'devoops',

    'components' => array(
        'widgetFactory'=>array(
            'widgets'=>array(
                'CActiveForm' => array(
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                ),
            ),
        ),
        'user' => array(
            'loginUrl' => '/admin/login'
        ),
    ),

    'params' => array(
        'defaultPageSize' => 10,
    ),
);