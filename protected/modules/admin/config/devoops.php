<?php
return array(
    'components' => array(
        'widgetFactory'=>array(
            'widgets'=>array(
                'GridView' => array(
                    'summaryText' => Yii::t('f','Showing {start} to {end} of {count} entries'),
                    'summaryCssClass' => 'dataTables_info',
                    'pagerCssClass' => 'dataTables_paginate paging_full_numbers',
                    'pager'=>array(
                        //'class'=>'webroot.themes.devoops.widgets.DevoopsLinkPager',
                        'cssFile'=>false,
                        'header'=>false,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                        'internalPageCssClass'=>'',
                        'firstPageCssClass'=>'prev',
                        'previousPageCssClass'=>'',
                        'nextPageCssClass'=>'',
                        'lastPageCssClass'=>'next',
                        'selectedPageCssClass'=>'active',
                        'hiddenPageCssClass'=>'disabled',
                    ),
                    'cssFile'=>false,
                    'htmlOptions'=>array(
                        'class'=>'row',
                    ),
                    'template'=>
                        '<div class="col-xs-12">
                            <div>
                                <div class="box-content no-padding">
                                    <div class="dataTables_wrapper form-inline" role="grid">
                                        {items}
                                        <div class="box-content">
                                            <div class="col-sm-6">
                                                <div class="dataTables_info">{summary}</div>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    {pager}
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>',
                    'itemsCssClass'=>'table table-bordered table-striped table-hover table-heading table-datatable dataTable',
                ),
                'CActiveForm' => array(
                    'htmlOptions' => array(
                        'class' => "form-horizontal",
                        'role' => "form",
                    ),
                ),
                'WDropDownList' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ),
                'WTextField' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                    'containerOptions' => array(
                        'class' => 'form-group',
                    ),
                ),
                'WLocationField' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ),
                'WPasswordField' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ),
                'WDateField' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ),
                'WTextArea' => array(
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                    'containerOptions' => array(
                        'class' => 'form-group',
                    ),
                ),
                'WRedactor' => array(
                    'containerOptions' => array(
                        'class' => 'form-group',
                    ),
                ),
            ),
        ),
    ),
);