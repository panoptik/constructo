<?php
return array(
    'components' => array(
        'widgetFactory'=>array(
            'widgets'=>array(
                'GridView' => array(
                    'summaryText' => Yii::t('f','Showing {start} to {end} of {count} entries'),
                    'summaryCssClass' => 'dataTables_info',
                    'pagerCssClass' => 'dataTables_paginate paging_full_numbers',
                    'pager'=>array(
                        'class'=>'webroot.themes.aquincum.widgets.AquincumLinkPager',
                        'cssFile'=>false,
                        'header'=>false,
                        'internalPageCssClass'=>'paginate_button',
                        'firstPageCssClass'=>'paginate_button first',
                        'previousPageCssClass'=>'paginate_button previous',
                        'nextPageCssClass'=>'paginate_button next',
                        'lastPageCssClass'=>'paginate_button last',
                        'selectedPageCssClass'=>'paginate_active',
                        'hiddenPageCssClass'=>'paginate_button_disabled',
                    ),
                    'cssFile'=>false,
                    'htmlOptions'=>array(
                        'class'=>'dataTables_wrapper',
                    ),
                    'template'=>
                        '<div class="widget">
                            <div class="shownpars">
                                {items}
                                <div class="fg-toolbar tableFooter">
                                    {summary}{pager}
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>',
                    'itemsCssClass'=>'dTable dataTable',
                    'afterAjaxUpdate'=>'function() { js: $("select, .check, .check :checkbox, input:radio, input:file").uniform(); }',
                ),
                'SortableCGridView' => array(
                    'orderField' => 'sort',
                    'allItemsInOnePage' => false,
                    'summaryText' => Yii::t('f','Showing {start} to {end} of {count} entries'),
                    'summaryCssClass' => 'dataTables_info',
                    'pagerCssClass' => 'dataTables_paginate paging_full_numbers',
                    'pager'=>array(
                        'class'=>'webroot.themes.aquincum.widgets.AquincumLinkPager',
                        'cssFile'=>false,
                        'header'=>false,
                        'internalPageCssClass'=>'paginate_button',
                        'firstPageCssClass'=>'paginate_button first',
                        'previousPageCssClass'=>'paginate_button previous',
                        'nextPageCssClass'=>'paginate_button next',
                        'lastPageCssClass'=>'paginate_button last',
                        'selectedPageCssClass'=>'paginate_active',
                        'hiddenPageCssClass'=>'paginate_button_disabled',
                    ),
                    'cssFile'=>false,
                    'htmlOptions'=>array(
                        'class'=>'dataTables_wrapper',
                    ),
                    'template'=>
                        '<div class="widget">
                            <div class="shownpars">
                                {items}
                                <div class="fg-toolbar tableFooter">
                                    {summary}{pager}
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>',
                    'itemsCssClass'=>'dTable dataTable',
                    'afterAjaxUpdate'=>'function() { js: $("select, .check, .check :checkbox, input:radio, input:file").uniform(); }',
                ),
                'CActiveForm' => array(
                    'htmlOptions'=>array(
                        'class'=>'main',
                    ),
                ),
            ),
        ),
    ),
);
