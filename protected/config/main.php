<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Constructo',

    'language' => 'en',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions.facebook.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'qwerty',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
        'admin',
	),

	// application components
	'components'=>array(

//        'mailer' => array(
//            'class' =>          'application.extensions.mailer.EMailer',
//            'pathViews' =>      'application.views.email',
//            'pathLayouts' =>    'application.views.email.layouts'
//        ),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'class' => 'WebUser',
            'loginUrl' => 'auth/login'
		),

        // setup auth manager
        'authManager' => array(
            'class' => 'AuthManager',
            'defaultRoles' => array('guest'),
        ),

        // support multi language support
        'request'=>array(
            'class'=>'DLanguageHttpRequest',
        ),

		'urlManager'=>array(
            // support multi language support
            'class'=>'DLanguageUrlManager',
            'showScriptName' => false,
			'urlFormat'=>'path',
			'rules'=>array(

                // gii rules
                'gii' => 'gii',
                'gii/<controller:\w+>'=>'gii/<controller>/index',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

        'viewRenderer' => array(
            'class' => 'ViewRenderer',
        ),

		'db'=>require(__DIR__ . DIRECTORY_SEPARATOR . 'db.php'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

        'image' => array(
            'class' => 'admin.extensions.imageapi.CImage',
        ),

    ),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',

        'uploadFolder' => 'webroot.uploads',
        'tmpUploadFolder' => 'webroot.uploads.tmp',
        'wysiwygImageFolder' => 'webroot.uploads.img',
        'uploadSizeLimit' => 2 * 1024 * 1024,

        'noimageUrl' => '/uploads/noimage.png',

        'facebook' => array(
            'appId' => '533610003425665',
            'secret' => '83ac48fff311e5b0990ad78ac18efe42',
        ),

        'languages' => array(
            'en' => 'English',
            'de' => 'Deutsch',
        ),

        'passwordRecoveryHashLifetime' => 60*60,
        'defaultLanguage' => 'en',
	),
);