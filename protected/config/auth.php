<?php
return array(

    // roles
    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'bizRule' => null,
        'data' => null
    ),
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'User',
        'children' => array(
            'guest', // extends from guest
        ),
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'user',  // allow (extend) all "user" privileges
            'updateAction',
            'deleteAction',
            'viewAction',
        ),
        'bizRule' => null,
        'data' => null
    ),

    // operations
    'updateAction' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Ability to execute CRUD Update Action',
        'bizRule' => null,
        'data' => null
    ),
    'deleteAction' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Ability to execute CRUD Delete Action',
        'bizRule' => null,
        'data' => null
    ),
    'viewAction' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Ability to execute CRUD View Action',
        'bizRule' => null,
        'data' => null
    ),
);