<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 25.03.14
 * Time: 10:46
 */

class AuthController extends FrontController{

    /**
     * Displays the login page
     */

    private function _getFacebookUser($accessToken){
        if ($accessToken){
            try {
                $facebook = new Facebook(Yii::app()->params['facebook']);
                $facebook->setAccessToken($accessToken);
                return $userData = $facebook->api('/me');
            } catch(FacebookApiException $e) {
                Yii::log(
                    'Facebook api error: ' . $e->getMessage(). ' ' . PHP_EOL .
                    'Token: ' . $accessToken . ' ' . PHP_EOL .
                    'Credentials: ' . print_r(Yii::app()->params['facebook'], true) . PHP_EOL .
                    'Trace: ' . PHP_EOL . $e->getTraceAsString(),
                    CLogger::LEVEL_ERROR, 'facebook');
                throw new CException('Facebook api error');
            }
        }
        return false;
    }

    private function _login($user, $checkPassword = true){
        $identity = new UserIdentity($user->email, $user->password);
        if ($identity->authenticate($checkPassword)) {
            Yii::app()->user->login($identity, 60 * 60 * 24 * 7);
            return true;
        }
        return false;
    }

    private function _encodeHash($data){
        return urlencode(Yii::app()->getSecurityManager()->encrypt(json_encode($data)));
    }

    private function _decodeHash($hash){
        $data = urldecode($hash);
        $data = Yii::app()->getSecurityManager()->decrypt($data);
        $data = json_decode($data, true);
        return $data;
    }

    private function _validateHashData($data){
        if ($data){
            if (is_array($data)){
                if (isset($data['userId']) && isset($data['callTime'])){
                    $userInfo = User::model()->findByPk($data['userId']);
                    //echo 'updated at '.strtotime($userInfo->updatedAt). '<br/> calltime at ' . $data['callTime'];
                    if ($userInfo
                        && ((time() - $data['callTime']) < Yii::app()->params['passwordRecoveryHashLifetime'])
                        && (strtotime($userInfo->updatedAt) - $data['callTime'] < 0)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function _sendEmail($type, $toEmail, $emailData) {
        switch ($type) {
            case 'register':
                $subject = 'Registration on deliqat';
                $template = '/email/registration';
                break;
            case 'passwordRecovery':
                $subject = 'Password recovery';
                $template = '/email/passwordRecovery';
                break;
            case 'passwordRecoveryConfirm':
                $subject = 'Your new password';
                $template = '/email/passwordRecoveryConfirm';
                break;
        }
        $body = $this->renderPartial($template, $emailData, true);
        return Tools::sendEmail($toEmail, $subject, $body);
    }

    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }

        Yii::app()->clientScript->registerScript(
            'myHideEffect',
            '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
            CClientScript::POS_READY
        );

        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()  {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionPasswordRecovery() {
        $model = new PasswordRecoveryForm();

        $hiddenVals = $formRows = $formPassRows = array();
        $userNotice = false;

        //even if not is set.
        $output = array(
            'wrongHash' => false,
        );


        $restoreData = Yii::app()->request->getParam('hash');
        if ($restoreData) {
            $model->setScenario('recovery');
        }

        //used for second step = check if user with email exists, send email with recovery data
        $formData = Yii::app()->request->getParam('PasswordRecoveryForm');
        if ($formData){
            $model->attributes = $formData;
        }


        if (isset($formData['email'])){
            if (!$user = User::model()->findByAttributes(array(),'email=:email', array(':email'=>$formData['email']))){
                $model -> addError('email','No user with such email');
                $formRows = array('email');
            } else {

                $emailData = array(
                    'userInfo' => $user,
                    'hash'=> $this->_encodeHash(array(
                        'userId' => $user->id,
                        'callTime' => time()
                    ))
                );

                if ($this->_sendEmail('passwordRecovery', $formData['email'], $emailData)){
                    $userNotice = 'Email with instructions is sent to your Email';
                }
            }
        }


        //third step - check data from URL (and if new pass is sent - save it)
        if ($restoreData){
            $output['wrongHash'] = true;
            $restoreData = $this->_decodeHash($restoreData);
            if ($this->_validateHashData($restoreData)){

                $formPassRows = array('newPassword', 'newPasswordConfirm');
                $hiddenVals = array('hash' => Yii::app()->request->getParam('hash'));
                $output['wrongHash'] = false;

                if (!isset($formData['email']) && isset($formData['newPassword'])){

                    if ($model->saveNewPassword($restoreData['userId'])){

                        $userInfo = User::model()->findByPk($restoreData['userId']);

                        $emailData = array(
                            'userInfo' => $userInfo,
                            'newPassword' => $formData['newPassword'],
                        );

                        if ($this->_sendEmail('passwordRecoveryConfirm', $userInfo->email, $emailData)){
                            Yii::app()->user->setFlash('success',"You have successfully restored password. You can log in now.");
                            $this->redirect('/auth/login');
                        }
                    }
                }
            }
        } else {
            //no hash so just show first-step
            if (!$formData) {
                $formRows = array('email');
            }
        }

        $output = array_merge($output, array(
            'model' => $model,
            'formRows' => $formRows,
            'formPassRows' => $formPassRows,
            'hiddenVals' => $hiddenVals,
            'userNotice' => $userNotice,
        ));

        $this->render('passwordRecovery', $output);
    }

    public function actionRegister() {
        $model = new User('register');

        $userData = $this->_getFacebookUser(Yii::app()->request->getParam('facebookToken'));

        if ($userPostData = Yii::app()->request->getParam('User')){
            $model->attributes = $userPostData;
            $model->passwordConfirm = $userPostData['passwordConfirm'];

            if(isset($userData) && !empty($userData)) {
                $model->facebookId = $userData['id'];
            }

            if ($model->save()){
                $this->_login($model, false);

                if ($this->_sendEmail('register', $model->email, array('userInfo' => $model))){
                    $userNotice = 'Email with instructions is sent to your Email';
                }

                $this->redirect(Yii::app()->homeUrl);
            }
        }

        if ($FB_data = Yii::app()->request->getParam('FB')){
            $model->attributes = $FB_data;
            Yii::app()->clientScript->registerScript(
                'showFBAlert',
                "setTimeout(function(){alertMessage('Enter Password to Finish Registration');}, 1000);",
                CClientScript::POS_READY
            );
        }

        $this->render('register',array('model'=>$model, 'facebookToken'=>Yii::app()->request->getParam('facebookToken')));
    }

    public function actionFacebook() {

        $authResponse = Yii::app()->request->getParam('authResponse');
        if (isset($authResponse['accessToken'])){
            $userData = $this->_getFacebookUser($authResponse['accessToken']);
        }

        $json = array('success'=>false);
        if(isset($userData) && !empty($userData)) {
            $json['userData'] = $userData;
            $user = User::model()->findByAttributes(array('facebookId'=> $userData['id']));
            if(!$user && isset($userData['email'])) {
                $user = User::model()->findByAttributes(array('email'=>$userData['email']));
                if(!$user) {
                    $json['need_registration'] = true;
                } else {
                    //update info
                    $user->facebookId = $userData['id'];
                    $res = $user->save();

                    if(!$res)
                        throw new CException('cannot save user' . "\n" . print_r($user->getErrors(), true));

                    //login user
                    if($this->_login($user, false)){
                        $json['success'] = true;
                        $json['redirect'] = Yii::app()->homeUrl;
                    }
                }
            } elseif($user) {
                if($this->_login($user, false)){
                    $json['success'] = true;
                    $json['redirect'] = Yii::app()->homeUrl;
                }
            }
            if($user) {
                // update user facebook token ?
            } else {
                // in this case there is no email from facebook received
                // that's why need to show email field in registration form
                // @todo show email field in registration form when not received email from facebook or etc.
                //throw new CException('user not exists' . "\n" . print_r($userData, true));
            }
        }else{
            $json['message'] = "No data from facebook";
            $json['need_registration'] = false;
        }

        echo CJSON::encode($json);
        Yii::app()->end();
    }

}