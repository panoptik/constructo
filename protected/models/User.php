<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $role
 * @property integer $status
 * @property string $firstName
 * @property string $lastName
 * @property string $facebookId
 * @property string $image
 * @property string $createdAt
 * @property string $updatedAt
 *
 * The followings are the available model relations:
 * @property Cart[] $carts
 */
class User extends ActiveRecord
{

    public $passwordConfirm = null;
    private $_passwordOriginal;
    private $_oldPassword;

    const ROLE_ADMIN = 'admin';
    const ROLE_PARTNER = 'partner';
    const ROLE_USER = 'user';

    const STATUS_BANNED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_PENDING = 3;

    /**
     * @var array
     */
    public static $statuses = array(
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_PENDING => 'Pending',
        self::STATUS_BANNED => 'Banned',
    );

    /**
     * @var array
     */
    public static $roles = array(
        self::ROLE_ADMIN => 'admin',
        self::ROLE_PARTNER => 'partner',
        self::ROLE_USER => 'user',
    );

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        return isset(self::$statuses[$this->status])?self::$statuses[$this->status]:'Unknown';
    }

    /**
     * @return string
     */
    public function getRoleTitle()
    {
        return isset(self::$roles[$this->role])?self::$roles[$this->role]:'Unknown';
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, firstName, lastName', 'required'),
			array('password', 'required','except'=>'edit'),
            array('password, passwordConfirm','safe','on'=>'edit'),

            array('email, password, firstName, lastName, image', 'length', 'max'=>255),
            array('status', 'numerical', 'integerOnly'=>true),

            array('password', 'length', 'min'=>6),

            array('facebookId', 'length', 'max'=>100),
            array('role', 'length', 'max'=>50),
            array('email', 'email'),

			array('passwordConfirm', "compare", "compareAttribute" => "password", 'on'=>'register,edit,add'),
            array('email', 'unique', 'caseSensitive'=>false,'on'=>'register,add,edit'),


			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, role, status, firstName, lastName, facebookId, image, createdAt, updatedAt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'carts' => array(self::HAS_MANY, 'Cart', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'passwordConfirm' => 'Confirm Password',
			'role' => 'Role',
			'status' => 'Status',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
			'facebookId' => 'Facebook',
			'image' => 'Image',
			'createdAt' => 'Created At',
			'updatedAt' => 'Updated At',
			'fullName' => 'Full Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('facebookId',$this->facebookId,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('createdAt',$this->createdAt,true);
		$criteria->compare('updatedAt',$this->updatedAt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterFind()
    {
        $this->_oldPassword = $this->password;
        return parent::afterFind();
    }

    protected function beforeValidate()
    {
        return parent::beforeValidate();
    }

    protected function beforeSave(){
        if ($this->scenario == 'edit'){
            if(!$this->password)
                $this->password = $this->_oldPassword;
            else
                $this->password = CPasswordHelper::hashPassword($this->password);
        }
        $this->_passwordOriginal = $this->password;

        if ($this->isNewRecord){
            $this->createdAt = new CDbExpression('NOW()');
            $this->password = CPasswordHelper::hashPassword($this->password);

            if ($this->getScenario() == 'register'){
                $this->role = self::ROLE_USER;
            }
        }
        $this->updatedAt = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    public function getFullName(){
        return ucfirst($this->firstName).' '.ucfirst($this->lastName);
    }
}
