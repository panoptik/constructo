<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PasswordRecoveryForm extends CFormModel
{
	public $email;
	public $newPassword;
	public $newPasswordConfirm;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			// username and password are required
			array('email', 'required', 'except' => 'recovery'),
            array('newPassword, newPasswordConfirm', 'length', 'min'=>6),
			array('newPassword, newPasswordConfirm', 'required', 'on'=>'recovery'),
            array('newPasswordConfirm', "compare", "compareAttribute" => "newPassword", 'on'=>'recovery'),
			array('email', 'email'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
		);
	}

    public function saveNewPassword($userId){
        $user = User::model()->findByPk($userId);

        if ($user){
            $user->setScenario('edit');
            $user->setAttribute('password',$this->newPassword);
            $user->setAttribute('passwordConfirm',$this->newPasswordConfirm);
            return $user->save();
        }
        return false;
    }
}
