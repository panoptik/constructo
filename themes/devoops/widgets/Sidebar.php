<?php
/**
 * Created by PhpStorm.
 * User: Abbice
 * Date: 26.03.14
 * Time: 13:30
 */

class Sidebar extends Widget {

    public $params = array(
        //array(name, url, icon, class)
    );

    public function run() {

        $keys = array('title','url','icon','class','isActive');

        $output = array();

        foreach ($this->params as $paramRow){

            $outputRow = array();
            foreach ($keys as $i => $key){


                $data = array();
                //== Yii::app()->request->url
                if (isset($paramRow[$i])){
                    switch ($key) {
                        case 'url' :
                            $outputRow[$key] = $this->controller->createUrl($paramRow[$i]);
                            break;
                        default:
                            $outputRow[$key] = $paramRow[$i];
                    }
                } else {
                    $outputRow[$key] = '' ;
                }

                $outputRow[$key] = (isset($paramRow[$i])) ? ($key == 'url') ? $this->controller->createUrl($paramRow[$i]) : $paramRow[$i] : '' ;
            }

            $outputRow['isActive'] = true;

            $hierarchy = array(
                Yii::app()->controller->module->id,
                Yii::app()->controller->id,
                Yii::app()->controller->action->id
            );

            $parsedUrlRaw = explode('/',$outputRow['url']);
            $parsedUrl = array();
            foreach ($parsedUrlRaw as $key => $node){
                if (!empty($node)){
                    $parsedUrl[] = $node;
                }
            }

            if (!isset($parsedUrl[1])){
                $parsedUrl[1] = Yii::app()->controller->module->defaultController;
            }

            if (!isset($parsedUrl[2])){
                $parsedUrl[2] = Yii::app()->controller->defaultAction;
            }

            foreach ($hierarchy as $key => $node){
                if (isset($parsedUrl[$key]) && $node != $parsedUrl[$key]){
                    $outputRow['isActive'] = false;
                }
            }

            $output['menuRows'][] = $outputRow;
        }

        $this->render('sidebar', $output);
    }
}