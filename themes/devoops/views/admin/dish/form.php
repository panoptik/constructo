<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-content">
                <?php $form=$this->beginWidget('CActiveForm'); ?>
                <div class="page-header"><?=Yii::t('f','Fields with {star} are required.', array(
                        '{star}'=>'<span class="required">*</span>',
                    ));?>
                    <?php echo $form->errorSummary($model, null, null, array('class'=>'errorSummary')); ?>
                </div>

                <? $this->widget('WLangBar'); ?>

                <?php $this->renderFields($this->formFields, $form, $model); ?>

                <? $this->widget('WUploader', array(
                    'multiple' => true,
                    'model' => $model,
                    'attribute' => 'images',
                    'relation' => 'images',
                    'relationAttribute' => 'image',
                    'form' => $form,
                )); ?>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-2">
                        <button type="submit" class="btn btn-primary btn-label-left">
                            <span><i class="fa fa-check"></i></span>
                            <?=Yii::t('f','Submit');?>
                        </button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
 