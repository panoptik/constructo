<? ini_set('display_errors',1);?>
<? $this->widget('WFlashMessages'); ?>
<? /** @var Dish[] $dishes */ ?>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <div class="box-name">
                <i class="fa fa-picture-o"></i>
                <span>Dashboard</span>
            </div>
            <div class="box-icons">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="expand-link">
                    <i class="fa fa-expand"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="no-move"></div>
        </div>
        <div id="simple_gallery" class="box-content">
        </div>
    </div>
</div>