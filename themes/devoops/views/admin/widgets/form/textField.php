<? /** @var WTextField $this */ ?>
<? /** @var ActiveRecord $model */ ?>
<? /** @var string $attributes */ ?>
<? /** @var array $htmlOptions */ ?>
<?=CHtml::openTag($this->containerTag, $this->containerOptions); ?>
    <?php echo $form->labelEx($model, $attribute, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-8">
        <?php echo $form->textField($model, $attribute, $htmlOptions); ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
<?=CHtml::closeTag($this->containerTag); ?>