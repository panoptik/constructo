<div class="form-group">
    <?php echo $form->labelEx($model, $attribute, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-8">
        <span <?=CHtml::renderAttributes($htmlOptions);?>><?=$text;?></span>
    </div>
</div>
