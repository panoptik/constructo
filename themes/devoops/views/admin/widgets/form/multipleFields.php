<div class="formRow multipleFields" data-modelname="<?=$modelname;?>" data-relation="<?=$this->relationAttribute;?>" data-label="<?=$this->attributeLabel;?>" data-attribute="<?=$this->attribute;?>" data-field="<?=$this->fieldAttribute;?>" data-parent="<?=$this->parentIndex;?>" data-prefix="<?=$parentPrefix;?>" id="<?=$this->uniqueId;?>">
    <div class="grid3">
        <?=CHtml::label($this->attributeLabel,'');?>
    </div>
    <div class="grid9">
    <? foreach($models as $index => $model): ?>
    <div class="fieldItem <?=$this->uniqueId;?>" data-index="<?=$index;?>" data-id="<?=$model['id'];?>">
        <?=CHtml::textField("{$parentPrefix}[{$this->fieldAttribute}][{$index}][{$this->attribute}]",$model[$this->attribute],array('id'=>false)); ?>
        <?=CHtml::hiddenField("{$parentPrefix}[{$this->fieldAttribute}][{$index}][id]",$model['id']); ?>
        <a href="javascript:{}" class="remove roundBtn"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
        <a href="javascript:{}" class="add roundBtn <?=$this->uniqueId;?>"><div class="glyph"><div class="fs1 iconb" data-icon=""></div></div></a>
    </div>
    <? endforeach; ?>
    </div>
    <div class="clear"></div>
</div>