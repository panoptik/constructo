<? /** @var array $languages */ ?>
<div id="lang-choose-bar">
    <? foreach($languages as $code => $name): ?>
    <div class="language-selector">
        <a class="<?=($code==Yii::app()->language)?'active':'';?>" href="javascript:{}" data-lang="<?=$code;?>">
            <img src="<?=$this->getIconUrl($code);?>" />
            <span><?=$name;?></span>
        </a>
    </div>
    <? endforeach; ?>
</div>