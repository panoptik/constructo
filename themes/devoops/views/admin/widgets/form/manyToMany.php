<div class="formRow">
    <div class="grid3"><?php echo $form->labelEx($model, $attribute); ?></div>
    <div class="grid9">
        <? foreach($listItems as $value => $title): ?>
            <div>
            <?=$form->checkBox($model, $attribute.'['.$value.']', array(
                'checked'=>$this->isChecked($value),
                'uncheckValue'=>null,
                'value'=>$value,
            )); ?>
            <?=$form->labelEx($model, $attribute.'['.$value.']', array('label'=>$title)); ?>
            </div>
        <? endforeach; ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
    <div class="clear"></div>
</div>