<div class="form-group">
    <?php echo $form->labelEx($model, $attribute, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-8">
        <?php echo $form->textField($model, $attribute, $htmlOptions); ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
</div>
<?php
$htmlOptions['readonly'] = 'readonly';
?>
<?foreach ($hiddenFields as $field): ?>
<div class="form-group">
    <?php echo $form->labelEx($model, $field, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-8">
        <?php echo $form->textField($model, $field, $htmlOptions); ?>
        <span class="note"><?php echo $form->error($model, $field); ?></span>
    </div>
</div>
<? endforeach ?>
<script>
    GOOGLE.model="Store";
    GOOGLE.field="address";
    GOOGLE.autocompleteLocation();
</script>