<div class="form-group">
    <?php echo $form->labelEx($model, '['.$this->uniqueId.']'.$attribute, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-2">
        <?php $this->widget('AjaxUpload', array(
            'model' => $model,
            'attribute' => $attribute,
            'multiple' => $this->multiple,
            'uniqueId' => $this->uniqueId,
            'relation' => $this->relation,
            'relationAttribute'=>$this->relationAttribute,
            'prefix' => $this->prefix,
        )); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <? if($this->multiple): ?>
            <div id="<?=CHtml::activeId($model, $this->relation);?>_<?=$this->uniqueId;?>_files">
                <? foreach($model->{$this->relation} as $relModel) {
                    $this->widget('WUploadedImage', array(
                        'model' => $model,
                        'attribute' => $attribute,
                        'relation' => $this->relation,
                        'relationAttribute' => $this->relationAttribute,
                        'multiple' => $this->multiple,
                        'prefix' => $this->prefix,
                        'image' => $relModel->{$this->relationAttribute},
                        'imgUrl' => $relModel->getImage($this->relationAttribute, 'x150'),
                    ));
                } ?>
            </div>
        <? else: ?>
            <div id="<?=CHtml::activeId($model, $attribute);?>_<?=$this->uniqueId;?>_files">
                <?php if($model->$attribute) {
                    $this->widget('WUploadedImage', array(
                        'model' => $model,
                        'attribute' => $attribute,
                        'multiple' => $this->multiple,
                        'prefix' => $this->prefix,
                    ));
                } ?>
            </div>
        <? endif; ?>
        <div class="clear"></div>
    </div>
</div>