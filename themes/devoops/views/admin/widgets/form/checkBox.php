<div class="form-group">
    <?php echo $form->labelEx($model, $attribute, array(
        'class' => 'col-sm-2 control-label',
    )); ?>
    <div class="col-sm-4">
        <?php echo $form->checkBox($model, $attribute, $htmlOptions); ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
</div>
