<? foreach($flashes as $m): ?>
    <div class="alert <?=$m['class']?> alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong><?=$m['key']?></strong> <?=$m['message']?>
    </div>
<? endforeach ?>