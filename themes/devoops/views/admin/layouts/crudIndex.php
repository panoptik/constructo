<? $this->widget('WFlashMessages'); ?>
<?php
if($this->addButton) {
    echo CHtml::link(Yii::t('f', 'Add'), $this->createUrl("add"), array('class'=>'btn btn-primary'));
}
$this->widget('GridView', $this->gridData);
