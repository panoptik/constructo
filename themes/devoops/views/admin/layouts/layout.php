<?php /** @var string $content */ ?>
<?php /** @var CClientScript $cs */ ?>
<?php $cs = Yii::app()->clientScript; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php

    // styles

    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/plugins/bootstrap/bootstrap.min.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/plugins/jquery-ui/jquery-ui.min.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/font-awesome.min.css');

    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/plugins/fancybox/jquery.fancybox.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/plugins/fullcalendar/fullcalendar.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/plugins/xcharts/xcharts.min.css');

    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/style.css');

    // scripts

//    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/plugins/jquery/jquery-2.1.0.min.js');
    $cs->registerCoreScript('jquery');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/plugins/jquery-ui/jquery-ui.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/plugins/bootstrap/bootstrap.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/devoops.js');
    ?>

    <meta charset="utf-8">
    <title><?=Yii::app()->name;?></title>
    <meta name="description" content="description">
    <meta name="author" content="DevOOPS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<?=$content;?>

</body>
</html>
