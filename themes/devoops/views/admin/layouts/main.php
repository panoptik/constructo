<?php /** @var string $content */ ?>
<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/layout'); ?>

<!--Start Header-->
<div id="screensaver">
    <canvas id="canvas"></canvas>
    <i class="fa fa-lock" id="screen_unlock"></i>
</div>
<div id="modalbox">
    <div class="devoops-modal">
        <div class="devoops-modal-header">
            <div class="modal-header-name">
                <span>Basic table</span>
            </div>
            <div class="box-icons">
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="devoops-modal-inner">
        </div>
        <div class="devoops-modal-bottom">
        </div>
    </div>
</div>
<header class="navbar">
    <div class="container-fluid expanded-panel">
        <div class="row">
            <div id="logo" class="col-xs-12 col-sm-2">
                <a href="<?=$this->createUrl('/admin');?>">Admin Area</a>
            </div>
            <div id="top-panel" class="col-xs-12 col-sm-10">
                <div class="row">
                    <div class="col-xs-8 col-sm-4">
                        <a href="#" class="show-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                        <div style="margin: 7px 0 0 58px;">
                            <h2><?=$this->title;?></h2>
                        </div>
<!--                        <div id="search">-->
<!--                            <input type="text" placeholder="search"/>-->
<!--                            <i class="fa fa-search"></i>-->
<!--                        </div>-->
                    </div>
                    <div class="col-xs-4 col-sm-8 top-panel-right">
                        <ul class="nav navbar-nav pull-right panel-menu">
                            <li class="">
                               <? $this->widget('admin.widgets.LanguageSwitcher.WLanguageSwitcher')?>
                            </li>
                            <?
                            /*
                            <li class="hidden-xs">
                                <a href="index.html" class="modal-link">
                                    <i class="fa fa-bell"></i>
                                    <span class="badge">7</span>
                                </a>
                            </li>
                            <li class="hidden-xs">
                                <a class="ajax-link" href="ajax/calendar.html">
                                    <i class="fa fa-calendar"></i>
                                    <span class="badge">7</span>
                                </a>
                            </li>
                            <li class="hidden-xs">
                                <a href="ajax/page_messages.html" class="ajax-link">
                                    <i class="fa fa-envelope"></i>
                                    <span class="badge">7</span>
                                </a>
                            </li>
                            */
                            ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                    <div class="avatar">
                                        <img src="<?=Yii::app()->user->getImage();?>" class="img-rounded" alt="avatar" />
                                    </div>
                                    <i class="fa fa-angle-down pull-right"></i>
                                    <div class="user-mini pull-right">
                                        <span class="welcome">Welcome,</span>
                                        <span><?=(!Yii::app()->user->isGuest && isset(Yii::app()->user->name)) ? Yii::app()->user->name : 'Guest';?></span>
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <?
                                    /*
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user"></i>
                                            <span class="hidden-sm text">Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/page_messages.html" class="ajax-link">
                                            <i class="fa fa-envelope"></i>
                                            <span class="hidden-sm text">Messages</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/gallery_simple.html" class="ajax-link">
                                            <i class="fa fa-picture-o"></i>
                                            <span class="hidden-sm text">Albums</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/calendar.html" class="ajax-link">
                                            <i class="fa fa-tasks"></i>
                                            <span class="hidden-sm text">Tasks</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-cog"></i>
                                            <span class="hidden-sm text">Settings</span>
                                        </a>
                                    </li>
                                    */
                                    ?>
                                    <li>
                                        <a href="<?=$this->createUrl('/auth/logout');?>">
                                            <i class="fa fa-power-off"></i>
                                            <span class="hidden-sm text">Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" class="container-fluid">
    <div class="row">
        <div id="sidebar-left" class="col-xs-2 col-sm-2">
            <?php $this->widget('webroot.themes.devoops.widgets.Sidebar', array(
                'params' => array(
                    array('Dashboard',  '/admin',           'fa-desktop'),
                    array('Users',      '/admin/user',      'fa-user'),
//                    array('Post',       '/admin/post',        'fa-edit'),
                    array('Settings',           '/admin/settings',          'fa-gear'),
                )
            )); ?>
        </div>
        <!--Start Content-->
        <div id="content" class="col-xs-12 col-sm-10">
            <div class="preloader" style="display: none;">
                <img src="<?=Yii::app()->theme->baseUrl;?>/assets/img/devoops_getdata.gif" class="devoops-getdata" alt="preloader"/>
            </div>
            <div class="row">
                <div id="breadcrumb" class="col-xs-12">
                    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                        'tagName'=>'ol',
                        'htmlOptions' => array(
                            'class'=>'breadcrumb',
                        ),
                        'homeLink'=>'<li><a href="'.$this->homeUrl.'">'.Yii::t('f','Home').'</a></li>',
                        'activeLinkTemplate' => "\n".'<li><a href="{url}">{label}</a></li>'."\n",
                        'inactiveLinkTemplate' => "\n".'<li><span>{label}</span></li>'."\n",
                        'links'=>$this->breadcrumbs,
                        'separator' => '',
                    )); ?>
                </div>
            </div>
            <?=$content; ?>

        </div>
        <!--End Content-->
    </div>
</div>

<?php $this->endContent(); ?>