$(function() {

	//===== Chosen plugin =====//
		
	$(".select").chosen(); 
	
	//===== Top panel search field =====//

	$('.userNav a.search').click(function () {
		$('.topSearch').fadeToggle(150);
	});

	//===== Animated dropdown for the right links group on breadcrumbs line =====//

	$('.breadLinks ul li').click(function () {
		$(this).children("ul").slideToggle(150);
	});
	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (! $clicked.parents().hasClass("has"))
		$('.breadLinks ul li').children("ul").slideUp(150);
	});

	//===== Add classes for sub sidebar detection =====//
	
	if ($('div').hasClass('secNav')) {
		$('#sidebar').addClass('with');
		//$('#content').addClass('withSide');
	}
	else {
		$('#sidebar').addClass('without');
		$('#content').css('margin-left','100px');//.addClass('withoutSide');
		$('#footer > .wrapper').addClass('fullOne');
		};

	//===== Button for showing up sidebar on iPad portrait mode. Appears on right top =====//
				
	$("ul.userNav li a.sidebar").click(function() { 
		$(".secNav").toggleClass('display');
	});


	//===== Form elements styling =====//

	$(".check, .check :checkbox, input:radio, input:file").uniform();
    $.uniform.update("input:checkbox, input:radio");


    // application logic
    //=============================================================//

    // allow submit button value send by ajax
    $('body').on('click', 'input:submit', function() {
        var input = '<input type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).attr('value')+'" />';
        $(this).closest('form').append(input)
    });

    // slider show/hide
    $('body').on('click', '.slide .toggleSlide', function() {
        $(this).siblings('.slideContent').slideToggle(200);
    });


});

function selectChosen(el) {
    el.find('select').chosen();
}

function translit(t, selector) {
    var o = $(t),
        text = o.val(),
        c = o.data('c'),
        url = '/admin/' + c + '/translit';


    $.get(url, {text: text}, function(data) {
        $(selector).val(data);
    });
}