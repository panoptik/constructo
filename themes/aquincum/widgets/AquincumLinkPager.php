<?php
/**
 * Created by JetBrains PhpStorm.
 * User: panoptik
 * Date: 13.06.13 Time: 1:24
 */

class AquincumLinkPager extends CLinkPager {

    public function run()
    {
        $this->registerClientScript();
        $buttons=$this->createPageButtons();
        if(empty($buttons))
            return;
        echo $this->header;
        echo implode("\n",$buttons);
        echo $this->footer;
    }

    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        if($hidden || $selected)
            $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        return CHtml::link($label,$this->createPageUrl($page), array('class'=>$class));
    }

}