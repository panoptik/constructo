<!-- Main content wrapper begins -->
<div class="errorWrapper">
    <span class="errorNum"><?=$code;?></span>
    <div class="errorContent">
        <span class="errorDesc"><span class="icon-warning"></span>Oops! Sorry, an error has occured. <?=$text;?>!</span>
        <? /*
        <div class="searchLine first">
            <form action="">
                <input type="text" name="search" class="ac" placeholder="Enter search text..." />
                <button type="submit" name="find" value=""><span class="icos-search"></span></button>
            </form>
        </div>
        */ ?>
        <div class="fluid">
            <a href="<?=$this->homeUrl;?>" title="" class="buttonM bLightBlue grid6">Back to dashboard</a>
            <a href="<?=Yii::app()->baseUrl;?>/" title="" class="buttonM bRed grid6">Back to the website</a>
        </div>
    </div>
</div>
<!-- Main content wrapper ends -->  