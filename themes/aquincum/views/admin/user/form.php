<?php $form=$this->beginWidget('CActiveForm'); ?>
    <fieldset>
        <div class="widget fluid">
            <div class="whead">
                <h6><?=Yii::t('f','Fields with {star} are required.', array(
                        '{star}'=>'<span class="required">*</span>',
                    ));?></h6>
                <?php echo $form->errorSummary($model, null, null, array('class'=>'errorSummary formRow')); ?>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <a href="<?=Yii::app()->createUrl('admin/project/add', array('userId'=>$model->id));?>" class="buttonM bBlue">Создать проект</a>
            </div>

            <?php
            $this->renderFields($this->formFields, $form, $model);

            $js = "$('#User_type').change(function(){
                $('fieldset.user_type').slideUp();
                $('#type_'+$(this).val()).slideDown();
            });
            $('#User_type').change();";
            /* @var CClientScript $cs */
            $cs = Yii::app()->clientScript;
            $cs->registerScript('user#type', $js, CClientScript::POS_READY);
            ?>

            <fieldset id="type_company" class="user_type form-row">
                <legend>Компания</legend>
                <?php
                $formFields = array(
                    'companyName' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companyDescription' => array(
                        'widgetClass' => 'WRedactor',
                    ),
                    'companyLogo' => array(
                        'widgetClass' => 'WUploader',
                    ),
                    'companyDirectorName' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companySite' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companyEDRPOU' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companyIPN' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companySertificate' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companyAddress' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'companyFoundingDate' => array(
                        'widgetClass' => 'WDateField',
                    ),
                );
                $this->renderFields($formFields, $form, $model);
                ?>
            </fieldset>

            <fieldset id="type_private_person" class="user_type form-row">
                <legend>Частное лицо</legend>
                <?php
                $formFields = array(
                    'birthDate' => array(
                        'widgetClass' => 'WDateField',
                    ),
                    'personPassportCode' => array(
                        'widgetClass' => 'WTextField',
                    ),
                    'personPassportNumber' => array(
                        'widgetClass' => 'WTextField',
                    ),
                );
                $this->renderFields($formFields, $form, $model);
                ?>
            </fieldset>

            <? if($projects = $model->projects): ?>
                <fieldset id="" class="form-row">
                    <legend>Проекты</legend>
                    <div class="formRow">
                        <div class="grid3"><?=CHtml::label(Yii::t('f','Projects'), ''); ?></div>
                        <div class="grid9">
                            <? foreach($projects as $project): ?>
                                <span>
                    <a href="<?=Yii::app()->createUrl('/admin/project/view', array('id'=>$project->id));?>">
                        <?=$project->getTitle();?>
                    </a>
                </span><br />
                            <? endforeach; ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
            <? endif; ?>



            <div class="formRow">
                <div class="grid3"></div>
                <div class="grid9">
                    <?php echo CHtml::submitButton(Yii::t('f','Submit'), array('class'=>'buttonM bBlack')); ?>
                    <?php echo CHtml::submitButton(Yii::t('f','Apply'), array(
                        'class'=>'buttonM bBlack',
                        'value'=>Yii::t('f','Apply'),
                        'name'=>'apply',
                    )); ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>