<div class="formRow">
    <div class="grid3"><?php echo $form->labelEx($model, $attribute); ?></div>
    <div class="grid9">
        <? $this->widget('ImperaviRedactorWidget', array(
            'model' => $model,
            'attribute' => $attribute,
            'options' => array(
                'imageUpload' => Yii::app()->controller->createUrl('uploadWysiwygImages'),
                'imageGetJson' => Yii::app()->controller->createUrl('getWysiwygImages'),
            ),
            'htmlOptions' => $htmlOptions,
        )); ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
    <div class="clear"></div>
</div>