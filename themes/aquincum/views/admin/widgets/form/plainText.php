<div class="formRow">
    <div class="grid3"><?php echo $form->labelEx($model, $attribute); ?></div>
    <div class="grid9">
        <span <?=CHtml::renderAttributes($htmlOptions);?>><?=$model->$attribute;?></span>
    </div>
    <div class="clear"></div>
</div>