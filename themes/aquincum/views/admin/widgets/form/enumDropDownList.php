<div class="formRow">
    <div class="grid3"><?php echo $form->labelEx($model, $attribute); ?></div>
    <div class="grid9">
        <?= Html::enumDropDownList($model, $attribute, $htmlOptions); ?>
        <span class="note"><?php echo $form->error($model, $attribute); ?></span>
    </div>
    <div class="clear"></div>
</div>