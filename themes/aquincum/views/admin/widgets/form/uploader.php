<div class="formRow">
    <div class="grid3"><?php echo $form->labelEx($model, '['.$this->uniqueId.']'.$attribute); ?></div>
    <div class="grid9">
        <?php $this->widget('AjaxUpload', array(
            'model' => $model,
            'attribute' => $attribute,
            'multiple' => $this->multiple,
            'uniqueId' => $this->uniqueId,
            'relation' => $this->relation,
            'relationAttribute'=>$this->relationAttribute,
            'prefix' => $this->prefix,
        )); ?>
    </div>
	<div class="clear"></div>
    <? if($this->multiple): ?>
        <div id="<?=CHtml::activeId($model, $this->relation);?>_<?=$this->uniqueId;?>_files">
            <? foreach($model->{$this->relation} as $relModel) {
                $this->widget('WUploadedImage', array(
                    'model' => $model,
                    'attribute' => $attribute,
                    'relation' => $this->relation,
                    'relationAttribute' => $this->relationAttribute,
                    'multiple' => $this->multiple,
                    'prefix' => $this->prefix,
                    'image' => $relModel->{$this->relationAttribute},
                    'imgUrl' => $relModel->getImage($this->relationAttribute, 'x150'),
                ));
            } ?>
        </div>
    <? else: ?>
        <div id="<?=CHtml::activeId($model, $attribute);?>_<?=$this->uniqueId;?>_files">
            <?php if($model->$attribute) {
                $this->widget('WUploadedImage', array(
                    'model' => $model,
                    'attribute' => $attribute,
                    'multiple' => $this->multiple,
                    'prefix' => $this->prefix,
                ));
            } ?>
        </div>
    <? endif; ?>
    <div class="clear"></div>
</div>