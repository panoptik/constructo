<?php $form=$this->beginWidget('CActiveForm'); ?>
    <fieldset>
        <div class="widget fluid">
            <div class="whead">
                <h6><?=Yii::t('f','Fields with {star} are required.', array(
                        '{star}'=>'<span class="required">*</span>',
                    ));?></h6>
                <?php echo $form->errorSummary($model, null, null, array('class'=>'errorSummary formRow')); ?>
                <div class="clear"></div>
            </div>

            <?php $this->renderFields($this->formFields, $form, $model); ?>

            <div class="formRow">
                <div class="grid3"></div>
                <div class="grid9">
                    <?php echo CHtml::submitButton(Yii::t('f','Submit'), array('class'=>'buttonM bBlack')); ?>
                    <?php echo CHtml::submitButton(Yii::t('f','Apply'), array(
                        'class'=>'buttonM bBlack',
                        'value'=>Yii::t('f','Apply'),
                        'name'=>'apply',
                    )); ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>