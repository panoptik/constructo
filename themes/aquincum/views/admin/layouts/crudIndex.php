<br />

<?php
if ($this->addButton) {
    if ($this->enableAjaxCrud) {
        $this->widget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'data_form',
            // additional javascript options for the dialog plugin
            'options' => array(
                'title' => Yii::t('f','Data form'),
                'autoOpen' => false,
                'minWidth' => '800',
                'height' => '700',
            ),
        ));
        echo CHtml::ajaxLink(Yii::t('f', 'Add'), $this->createUrl("add"), array(
            'type' => 'post',
            'update' => '#data_form',
            'complete' => 'js:function(){$("#data_form").dialog("open");}',
        ), array(
            'class'=>"buttonM bBlue",
        ));

        //
        $js = "$('body').on('click', '#{$this->id}-grid a.update', function(e) {
            $.post($(this).attr('href'), function(data){
                 $('#data_form').html(data);
                 $('#data_form').dialog('open');
            });
            e.preventDefault();
        });
        $('#data_form').on('submit', 'form', function(e) {
            $.post($(this).attr('action'), $(this).serialize(), function(data) {
                if(data == 'success') {
                    $('#data_form').dialog('close');
                    $('#{$this->id}-grid').yiiGridView('update');
                } else if(data == 'apply') {
                    $('#{$this->id}-grid').yiiGridView('update');
                    alert('Save');
                } else {
                    $('#data_form').html(data);
                }
            });
            e.preventDefault();
        });
        ";
        Yii::app()->clientScript->registerScript('crud#form', $js, CClientScript::POS_READY);
    } else {
        echo CHtml::link(Yii::t('f', 'Add'), $this->createUrl("add"), array(
            'class'=>"buttonM bBlue",
        ));
    }
}
$this->widget('GridView',$this->gridData); ?>
