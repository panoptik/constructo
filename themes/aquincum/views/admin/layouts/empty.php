<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Aquincum - premium admin theme</title>

    <link href="<?=Yii::app()->theme->baseUrl;?>/admin/css/styles.css" rel="stylesheet" type="text/css" />
    <!--[if IE]> <link href="<?=Yii::app()->theme->baseUrl;?>/admin/css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <? /*
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/ui.spinner.js"></script>
 */ ?>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.mousewheel.js"></script>

    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/files/timepicker.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.chosen.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.uniform.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/files/functions.js"></script>

</head>

<body>
<?=$content;?>
</body>
</html>