<?php $this->beginContent('/layouts/admin'); ?>
<!-- Sidebar begins -->
<div id="sidebar">
    <div class="mainNav">
        <div class="user">
            <a title="" class="leftUserDrop">
                <img style="max-width: 80px;" src="<?=Yii::app()->theme->baseUrl;?>/admin/images/userLogin2.png" alt="" />
                <span><strong>3</strong></span>
            </a>
            <span><?=Yii::app()->user->role;?></span>
            <ul class="leftUser">
                <li><a href="#" title="" class="sProfile">My profile</a></li>
                <li><a href="#" title="" class="sMessages">Messages</a></li>
                <li><a href="#" title="" class="sSettings">Settings</a></li>
                <li><a href="#" title="" class="sLogout">Logout</a></li>
            </ul>
        </div>

        <!-- Responsive nav -->
        <div class="altNav">
            <div class="userSearch">
                <form action="">
                    <input type="text" placeholder="search..." name="userSearch" />
                    <input type="submit" value="" />
                </form>
            </div>

            <!-- User nav -->
            <ul class="userNav">
                <li><a href="#" title="" class="profile"></a></li>
                <li><a href="#" title="" class="messages"></a></li>
                <li><a href="#" title="" class="settings"></a></li>
                <li><a href="#" title="" class="logout"></a></li>
            </ul>
        </div>

        <!-- Main nav -->
        <?php $this->widget('zii.widgets.CMenu', array(
            'htmlOptions' => array(
                'class' => 'nav',
            ),
            'encodeLabel' => false,
            'items' => array(
                array('url'=>array('/admin/index/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/dashboard.png" alt="" /><span>'.Yii::t('f','Dashboard').'</span>'),
                array('url'=>array('/admin/user/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/ui.png" alt="" /><span>'.Yii::t('f','Users').'</span>'),
                array('url'=>array('/admin/service/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/forms.png" alt="" /><span>'.Yii::t('f','Services').'</span>', 'items' => array(
                        array('url'=>array('/admin/event/index'), 'label'=>Yii::t('f','Events')),
                        array('url'=>array('/admin/servicecategory/index'), 'label'=>Yii::t('f','Service Categories')),
                        array('url'=>array('/admin/metric/index'), 'label'=>Yii::t('f','Metrics')),
                    ),
                'active'=>in_array($this->id, array('servicecategory','metric','service','event'))),
                array('url'=>array('/admin/contractor/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/ui.png" alt="" /><span>'.Yii::t('f','Contractors').'</span>'),
                array('url'=>array('/admin/page/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/forms.png" alt="" /><span>'.Yii::t('f','Pages').'</span>', 'items'=>array(
                    array('url'=>array('/admin/article/index'), 'label'=>Yii::t('f','Articles')),
                    array('url'=>array('/admin/articlecategory/index'), 'label'=>Yii::t('f','Article Categories')),
                    array('url'=>array('/admin/articleissue/index'), 'label'=>Yii::t('f','Article Issues')),
                    array('url'=>array('/admin/news/index'), 'label'=>Yii::t('f','News')),
                    array('url'=>array('/admin/action/index'), 'label'=>Yii::t('f','Actions')),
                ),
                'active'=>in_array($this->id, array('articleissue','article','page','action','news', 'articlecategory')),),
                array('url'=>array('/admin/faq/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/tables.png" alt="" /><span>'.Yii::t('f','Faq').'</span>', 'items'=>array(
                        array('url'=>array('/admin/faqcategory/index'), 'label'=>Yii::t('f','Faq Category')),
                    ),
                    'active'=>in_array($this->id, array('faqcategory','faq'))),
                array('url'=>array('/admin/project/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/tables.png" alt="" /><span>'.Yii::t('f','Projects').'</span>', 'items'=>array(
                    array('url'=>array('/admin/task/index'), 'label'=>Yii::t('f','Tasks')),
                    array('url'=>array('/admin/tasktype/index'), 'label'=>Yii::t('f','Task types')),
                    array('url'=>array('/admin/cash/index'), 'label'=>Yii::t('f','Cash')),
                    array('url'=>array('/admin/costitem/index'), 'label'=>Yii::t('f','Cost Items')),
                ),
                'active'=>in_array($this->id, array('task','tasktype','project', 'costitem', 'cash')),),
                array('url'=>array('/admin/feedback/index'), 'label'=>'<img src="'.Yii::app()->theme->baseUrl.'/admin/images/icons/mainnav/forms.png" alt="" /><span>'.Yii::t('f','Feedback').'</span>', 'items'=>array(
                    array('url'=>array('/admin/contact/index'), 'label'=>Yii::t('f','Contact')),
                    array('url'=>array('/admin/callback/index'), 'label'=>Yii::t('f','Callback')),
                ),
                    'active'=>in_array($this->id, array('contact','callback','feedback')),),
            ),
        )); ?>

    </div>


</div>
<!-- Sidebar ends -->

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><span class="icon-<?=$this->name;?>"></span><?=$this->title;?></span>
        <ul class="quickStats">
            <li>
                <a href="" class="blueImg"><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/icons/quickstats/plus.png" alt="" /></a>
                <div class="floatR"><strong class="blue">5489</strong><span>visits</span></div>
            </li>
            <li>
                <a href="<?=Yii::app()->createUrl('/admin/user/index');?>" class="redImg"><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/icons/quickstats/user.png" alt="" /></a>
                <div class="floatR"><strong class="blue"><?=User::model()->count();?></strong><span><?=Yii::t('f','users');?></span></div>
            </li>
            <li>
                <a href="<?=Yii::app()->createUrl('/admin/order/index');?>" class="greenImg"><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/icons/quickstats/money.png" alt="" /></a>
                <div class="floatR"><strong class="blue"><?=Order::model()->count();?></strong><span><?=Yii::t('f','orders');?></span></div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>

    <!-- Breadcrumbs line -->
    <div class="breadLine">
        <div class="bc">
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'tagName'=>'ul',
                'htmlOptions' => array(
                    'id'=>'breadcrumbs',
                    'class'=>'breadcrumbs',
                ),
                'homeLink'=>'<li><a href="'.$this->homeUrl.'">'.Yii::t('f','Dashboard').'</a></li>',
                'activeLinkTemplate' => "\n".'<li><a href="{url}">{label}</a></li>'."\n",
                'inactiveLinkTemplate' => "\n".'<li class="current"><a href="">{label}</a></li>'."\n",
			    'links'=>$this->breadcrumbs,
			    'separator' => '',
		)); ?><!-- breadcrumbs -->
        </div>

        <div class="breadLinks">
            <ul>
                <li><a href="<?=Yii::app()->createUrl('/admin/order/index');?>" title=""><i class="icos-list"></i><span><?=Yii::t('f','Orders');?></span> <strong>(+<?=Order::model()->notViewed()->count();?>)</strong></a></li>
                <li><a href="<?=Yii::app()->createUrl('/admin/task/index');?>" title=""><i class="icos-check"></i><span><?=Yii::t('f','Tasks');?></span> <strong>(+<?=Task::model()->pending()->count();?>)</strong></a></li>
                <li class="has">
                    <a title="">
                        <i class="icos-money3"></i>
                        <span>Invoices</span>
                        <span><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/elements/control/hasddArrow.png" alt="" /></span>
                    </a>
                    <ul>
                        <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
                        <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
                        <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>

    <? if(Yii::app()->user->hasFlash('successMessage')): ?>
    <div class="success message">
        <?=Yii::app()->user->getFlash('successMessage'); ?>
    </div>
    <? endif; ?>

    <!-- Main content -->
    <div class="wrapper">
        <?=$content;?>
    </div>
    <!-- Main content ends -->
</div>
<!-- Content ends -->
<?php $this->endContent(); ?>

