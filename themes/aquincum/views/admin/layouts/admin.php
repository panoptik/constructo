<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Aquincum - premium admin theme</title>

    <link href="<?=Yii::app()->theme->baseUrl;?>/admin/css/styles.css" rel="stylesheet" type="text/css" />
    <!--[if IE]> <link href="<?=Yii::app()->theme->baseUrl;?>/admin/css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<? /*
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/ui.spinner.js"></script>
 */ ?>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.mousewheel.js"></script>

    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/files/timepicker.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.chosen.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/plugins/forms/jquery.uniform.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->theme->baseUrl;?>/admin/js/files/functions.js"></script>

</head>

<body>

<!-- Top line begins -->
<div id="top">
    <div class="wrapper">
        <a href="<?=$this->homeUrl;?>" title="" class="logo"><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/logo.png" alt="" /></a>

        <!-- Right top nav -->
        <? if(!Yii::app()->user->isGuest): ?>
        <div class="topNav">
            <ul class="userNav">
                <li><a title="" class="search"></a></li>
                <li><a href="/" title="Go to site" target="_blank" class="screen"></a></li>
                <li><a href="<?=Yii::app()->createUrl('/admin/config/index');?>" title="" class="settings"></a></li>
                <li><a href="<?=Yii::app()->createUrl('site/logout');?>" title="" class="logout"></a></li>
                <li class="showTabletP"><a href="#" title="" class="sidebar"></a></li>
            </ul>
            <a title="" class="iButton"></a>
            <div class="topSearch">
                <div class="topDropArrow"></div>
                <form action="">
                    <input type="text" placeholder="search..." name="topSearch" />
                    <input type="submit" value="" />
                </form>
            </div>
        </div>
        <? endif; ?>

        <!-- Responsive nav -->
        <ul class="altMenu">
            <li><a href="index.html" title="">Dashboard</a></li>
            <li><a href="ui.html" title="" class="exp">UI elements</a>
                <ul>
                    <li><a href="ui.html">General elements</a></li>
                    <li><a href="ui_icons.html">Icons</a></li>
                    <li><a href="ui_buttons.html">Button sets</a></li>
                    <li><a href="ui_grid.html">Grid</a></li>
                    <li><a href="ui_custom.html">Custom elements</a></li>
                    <li><a href="ui_experimental.html">Experimental</a></li>
                </ul>
            </li>
            <li><a href="forms.html" title="" class="exp">Forms stuff</a>
                <ul>
                    <li><a href="forms.html">Inputs &amp; elements</a></li>
                    <li><a href="form_validation.html">Validation</a></li>
                    <li><a href="form_editor.html">File uploads &amp; editor</a></li>
                    <li><a href="form_wizards.html">Form wizards</a></li>
                </ul>
            </li>
            <li><a href="messages.html" title="">Messages</a></li>
            <li><a href="statistics.html" title="">Statistics</a></li>
            <li><a href="tables.html" title="" class="exp">Tables</a>
                <ul>
                    <li><a href="tables.html">Standard tables</a></li>
                    <li><a href="tables_dynamic.html">Dynamic tables</a></li>
                    <li><a href="tables_control.html">Tables with control</a></li>
                    <li><a href="tables_sortable.html">Sortable &amp; resizable</a></li>
                </ul>
            </li>
            <li><a href="other_calendar.html" title="" class="exp" id="current">Other pages</a>
                <ul>
                    <li><a href="other_calendar.html">Calendar</a></li>
                    <li><a href="other_gallery.html">Images gallery</a></li>
                    <li><a href="other_file_manager.html">File manager</a></li>
                    <li><a href="other_404.html" class="active">Sample error page</a></li>
                    <li><a href="other_typography.html">Typography</a></li>
                </ul>
            </li>
        </ul>

        <div class="clear"></div>
    </div>
</div>
<!-- Top line ends -->

<?=$content; ?>

</body>
</html>
