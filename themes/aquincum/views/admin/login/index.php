<!-- Login wrapper begins -->
<div class="loginWrapper">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    <div class="loginPic">
        <a href="#" title=""><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/userLogin2.png" alt="" /></a>
        <? /*
        <span>Hi, user</span>
        <div class="loginActions">
            <div><a href="#" title="Change user" class="logleft flip"></a></div>
            <div><a href="#" title="Forgot password?" class="logright"></a></div>
        </div>
        */ ?>
    </div>

    <?php echo $form->textField($model,'username', array(
        'class'=>'loginEmail',
        'placeholder'=>Yii::t('f', 'Your email'),
    )); ?>
    <?php echo $form->error($model,'username'); ?>

    <?php echo $form->passwordField($model,'password', array(
        'class'=>'loginPassword',
        'placeholder'=>Yii::t('f', 'Password'),
    )); ?>
    <?php echo $form->error($model,'password'); ?>

    <div class="logControl">
        <div class="memory">
        <?php echo $form->checkBox($model,'rememberMe', array(
            'class' => 'check',
            'checked' => true,
        )); ?>
        <?php echo $form->label($model,'rememberMe'); ?>
        </div>
        <?php echo $form->error($model,'rememberMe'); ?>
        <?php echo CHtml::submitButton('Login', array(
            'class'=>'buttonM bBlue',
            'value'=>Yii::t('f','Login'),
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

    <? /*
    <!-- Current user form -->
    <form action="index.html" id="login">
        <div class="loginPic">
            <a href="#" title=""><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/userLogin.png" alt="" /></a>
            <span>Eugene Kopyov</span>
            <div class="loginActions">
                <div><a href="#" title="Change user" class="logleft flip"></a></div>
                <div><a href="#" title="Forgot password?" class="logright"></a></div>
            </div>
        </div>

        <input type="text" name="login" placeholder="Confirm your email" class="loginEmail" />
        <input type="password" name="password" placeholder="Password" class="loginPassword" />

        <div class="logControl">
            <div class="memory"><input type="checkbox" checked="checked" class="check" id="remember1" /><label for="remember1">Remember me</label></div>
            <input type="submit" name="submit" value="Login" class="buttonM bBlue" />
            <div class="clear"></div>
        </div>
    </form>

    <!-- New user form -->
    <form action="index.html" id="recover">
    <div class="loginPic">
    <a href="#" title=""><img src="<?=Yii::app()->theme->baseUrl;?>/admin/images/userLogin2.png" alt="" /></a>
    <div class="loginActions">
    <div><a href="#" title="" class="logback flip"></a></div>
    <div><a href="#" title="Forgot password?" class="logright"></a></div>
</div>
</div>

    <input type="text" name="login" placeholder="Your username" class="loginUsername" />
    <input type="password" name="password" placeholder="Password" class="loginPassword" />

    <div class="logControl">
    <div class="memory"><input type="checkbox" checked="checked" class="check" id="remember2" /><label for="remember2">Remember me</label></div>
    <input type="submit" name="submit" value="Login" class="buttonM bBlue" />
</div>
    </form>
*/ ?>
</div>
<!-- Login wrapper ends -->